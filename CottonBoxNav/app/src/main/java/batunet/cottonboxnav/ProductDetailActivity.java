package batunet.cottonboxnav;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import batunet.cottonboxnav.adapters.ProductsImageAdapter;
import batunet.cottonboxnav.models.Product;
import batunet.cottonboxnav.models.Sets;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onuryurtturk on 5/15/2015.
 */
public class ProductDetailActivity extends ActionBarActivity {


    Context context;
    private static String original_url;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageView productView;
    private ViewPager product_slider;
    private ArrayList<Sets> sets;
    private ProductsImageAdapter products_image_adapter;
    private CirclePageIndicator seriesIndicator;
    String detail_name,product_name;
    SharedPrefUtils utils;
    int position=0;
    TextView actionText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_series_detail);
        context = this;
        original_url = getIntent().getStringExtra("original_url");
        utils = new SharedPrefUtils(context);
        //sets = utils.getPrefProduct();
        sets = (ArrayList<Sets>)getIntent().getSerializableExtra("set_list");
        detail_name = getIntent().getStringExtra("detail_name");
        position = getIntent().getIntExtra("position",0);
        product_name = getIntent().getStringExtra("product_name");
        product_slider = (ViewPager)findViewById(R.id.series_pager);
        products_image_adapter = new ProductsImageAdapter(context, sets);
        product_slider.setAdapter(products_image_adapter);
        product_slider.setCurrentItem(position);

        initActionBar();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        View view =getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(product_name);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
