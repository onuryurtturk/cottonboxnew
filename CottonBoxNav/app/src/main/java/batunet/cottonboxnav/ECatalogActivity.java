package batunet.cottonboxnav;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.ECatalogAdapter;
import batunet.cottonboxnav.models.CatalogF;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 28.7.2015.
 */
public class ECatalogActivity extends ActionBarActivity {


    Context context;
    ArrayList<String> pages, org_pages;
    String cat_name;
    static final int progress_bar_type = 0;
    CatalogF currentCatalog;
    int checkLenght = 0;
    SharedPrefUtils utils;
    ViewPager catalog_pager;
    CirclePageIndicator catIndicator;
    ECatalogAdapter cat_adapter;
    public ProgressDialog pDialog;
    NetworkOperations operations;
    ContextWrapper contextWrapper;
    TextView actionText;
    AlertDialog.Builder alert;
    SeekBar pager_seekbar;
    TextView pager_number;
    private int lastPage = 0;

    @Override
    public void onBackPressed() {
        Intent mIntent = new Intent();
        mIntent.putExtra("downloaded", 1);
        setResult(RESULT_OK, mIntent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_catalog_pager);
        context = this;
        contextWrapper = new ContextWrapper(context);
        catalog_pager = (ViewPager) findViewById(R.id.catalog_detail_pager);
        //catIndicator = (CirclePageIndicator) findViewById(R.id.series_indicator);
        pager_number = (TextView)findViewById(R.id.catalog_page_number);
        pager_seekbar = (SeekBar)findViewById(R.id.catalog_detail_bar);
        operations = new NetworkOperations(context);
        currentCatalog = (CatalogF) getIntent().getSerializableExtra("catalog");
        pages = currentCatalog.getPages();
        org_pages = currentCatalog.getOriginal_pages();
        cat_name = currentCatalog.getName();
        utils = new SharedPrefUtils(context);
        alert = new AlertDialog.Builder(context);
        pager_seekbar.setMax(currentCatalog.page_count);


        initActionBar();
        // File direct = new File(Environment.getExternalStorageDirectory() + "/Cotton/" + cat_name);
        File direct = new File(contextWrapper.getFilesDir().getPath() + "/.Cotton/" + cat_name);


        if (currentCatalog.is_downloaded) {


            int count = getDirPageCount(direct);
            if (currentCatalog.page_count == getDirPageCount(direct)) {
                Toast.makeText(context, getResources().getString(R.string.catalog_download_warning), Toast.LENGTH_LONG).show();
                cat_adapter = new ECatalogAdapter(context, pages);
                catalog_pager.setAdapter(cat_adapter);
            } else {
                Toast.makeText(context, getResources().getString(R.string.catalog_redownload), Toast.LENGTH_LONG).show();
                deleteDir(direct);
                new downloadTask().execute();
            }
        } else {
            if (operations.isNetworkConnected()) {
                new downloadTask().execute();
            } else {
                showAlert();
                //Toast.makeText(context, getResources().getString(R.string.language_warning_net), Toast.LENGTH_LONG).show();
            }

        }


        pager_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                catalog_pager.setCurrentItem(progress);
                pager_number.setText(String.valueOf(progress+1));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        catalog_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (lastPage > position) {


                } else if (lastPage < position) {

                    lastPage = position;
                }

                pager_seekbar.setProgress(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



    }



    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        View view = getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView) view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.title_catalog_activity));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    private void showAlert() {

        alert.setTitle(context.getResources().getString(R.string.catalog_error_title));
        alert.setMessage(context.getResources().getString(R.string.catalog_error));
        alert.setCancelable(false);
        alert.setNeutralButton(context.getResources().getString(R.string.map_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent mIntent = new Intent();
                mIntent.putExtra("downloaded", 0);
                setResult(RESULT_OK, mIntent);
                finish();
            }
        });
        alert.show();

    }


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage(getResources().getString(R.string.catalog_download));
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgress(0);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Intent mIntent = new Intent();
            mIntent.putExtra("downloaded", 1);
            setResult(RESULT_OK, mIntent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }


    public int getDirPageCount(File dir) {
        String[] children = {};
        if (dir.isDirectory()) {
            children = dir.list();
        }

        return children.length;
    }


    private class downloadTask extends AsyncTask<String, String, JSONObject> {

        JSONObject catalog = null;
        int total = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected JSONObject doInBackground(String... args) {


            try {

                if (!(pages.size() == 0)) {

                   /* File direct = new File(Environment.getExternalStorageDirectory()
                            + "/Cotton/" + cat_name);*/
                    File direct = new File(contextWrapper.getFilesDir().getPath() + "/.Cotton/" + cat_name);
                    if (!direct.exists()) {
                        direct.mkdirs();
                    }


                    URL url = null;
                    int fileLength;

                    for (int i = 0; i < pages.size(); i++) {


                        try {
                            url = new URL(org_pages.get(i));
                            URLConnection connection = url.openConnection();
                            connection.connect();
                            fileLength = connection.getContentLength();
                            InputStream input = new BufferedInputStream(url.openStream());
                            OutputStream output = new FileOutputStream(direct + "/" + "catalog" + String.valueOf(i) + ".jpg");

                            byte data[] = new byte[1024];
                            long total = 0;
                            int count;
                            while ((count = input.read(data)) != -1) {
                                total += count;
                                //publishProgress("" + (int) (total * 100 / fileLength));
                                output.write(data, 0, count);
                            }

                            output.flush();
                            output.close();
                            input.close();


                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        pages.set(i, ("file://" + direct + "/" + "catalog" + String.valueOf(i) + ".jpg"));
                        publishProgress("" + (int) ((i * 100) / pages.size()));
                        checkLenght++;

                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return catalog;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            pDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(JSONObject json) {


            /*currentCatalog.setIs_downloaded(true);
            ArrayList<CatalogF> catalogFs = new ArrayList<>();
            catalogFs.add(currentCatalog);
            utils.savePrefCatalog(catalogFs);
            dismissDialog(progress_bar_type);

            cat_adapter = new ECatalogAdapter(context, pages);
            catalog_pager.setAdapter(cat_adapter);*/
            //String a = org_pages.get(0);
            currentCatalog.setIs_downloaded(true);
            ArrayList<CatalogF> catalogFs = new ArrayList<>();
            catalogFs.add(currentCatalog);
            utils.savePrefCatalog(catalogFs);
            dismissDialog(progress_bar_type);

            cat_adapter = new ECatalogAdapter(context, pages);
            catalog_pager.setAdapter(cat_adapter);

          /*  onComplete = new BroadcastReceiver() {
                public void onReceive(Context ctxt, Intent intent) {
                    Log.e("Download Completed", "DOWNLOAD");
                    //hidePDialog();
               /* if(pages.size()==checkLenght)
                {*/


              /*  currentCatalog.setIs_downloaded(true);
                ArrayList<CatalogF> catalogFs = new ArrayList<>();
                catalogFs.add(currentCatalog);
                utils.savePrefCatalog(catalogFs);
                dismissDialog(progress_bar_type);

                cat_adapter = new ECatalogAdapter(context, pages);
                catalog_pager.setAdapter(cat_adapter);



                    //  }


                }
            };*/

        }


    }

}