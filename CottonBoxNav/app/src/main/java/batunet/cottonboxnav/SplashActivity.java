package batunet.cottonboxnav;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import batunet.cottonboxnav.models.Channel;
import batunet.cottonboxnav.models.Collection;
import batunet.cottonboxnav.models.Language;
import batunet.cottonboxnav.models.News;
import batunet.cottonboxnav.models.Product;
import batunet.cottonboxnav.models.Series;
import batunet.cottonboxnav.models.Sets;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 17.5.2015.
 */
public class SplashActivity extends Activity {

    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private final int KILL_TIME_LENGTH = 7000;
    Context context;
    public static ArrayList<Collection> allCollection;
    public static ArrayList<Product> allProduct;
    public static ArrayList<Sets> allSets;
    public static ArrayList<Channel> allChannel;
    public static ArrayList<News> allNews, allPress;
    public static ArrayList<Language> allLanguages;
    int language_id;
    String language_name, language_shortname;

    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ProgressBar loadingBar;
    NetworkOperations operations;
    SharedPrefUtils utils;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;

        initArrayLists();
        operations = new NetworkOperations(context);
        utils = new SharedPrefUtils(context);
        loadingBar = (ProgressBar) findViewById(R.id.loadingBar);

        startAnimation(R.id.splash_logo, R.anim.fade_in, 3000);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // if (utils.getFirstRun() == 0)
        if (utils.getLanguageState() == 0) {
            Intent i = new Intent(this, LanguageActivity.class);
            startActivityForResult(i, 1);
            //Gelen Result'ı sharedPref'e kaydet
        } else {

            //SharedPreften değeri getir.
            //Aşağıdaki bloğu kullan

            ArrayList<Language> pref_languages = utils.getPrefLanguage();
            language_id = pref_languages.get(0).getLanguage_id();
            language_name = pref_languages.get(0).getLanguage_title();
            language_shortname = pref_languages.get(0).getLanguage_short();
            changeLocale(language_shortname);
            checkAppState();

        }
    }

    private void checkAppState() {
        if (operations.isNetworkConnected() && utils.getFirstRun() == 0) {
            new getAllProducts().execute();
            //utils.saveFirstRun(1);
        } else if (operations.isNetworkConnected()) {
            new getAllProducts().execute();

        } else {

            int first =utils.getFirstRun();
            int lstate =utils.getLanguageState();
            int offline = utils.getOfflineMode();
            if (!operations.isNetworkConnected() && utils.getFirstRun() == 0) {
                Toast.makeText(context, getResources().getString(R.string.splash_warning_data), Toast.LENGTH_LONG).show();
                killApp();
            } else if (!operations.isNetworkConnected() && utils.getOfflineMode() == 1) {
                startApp();
            } else if (operations.isNetworkConnected()) {
                startApp();
            } else if (!operations.isNetworkConnected() && utils.getFirstRun() == 1 && utils.getLanguageState() == 1) {
                startApp();
            } else {
                Toast.makeText(context, getResources().getString(R.string.splash_warning_net), Toast.LENGTH_LONG).show();
                killApp();
            }
        }
    }

    private void changeLocale(String locale_name) {
        Locale locale = new Locale(locale_name);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

    private void initArrayLists() {
        allCollection = new ArrayList<>();
        allProduct = new ArrayList<>();
        allSets = new ArrayList<>();
        allChannel = new ArrayList<>();
        allNews = new ArrayList<>();
        allPress = new ArrayList<>();
        allLanguages = new ArrayList<>();

    }

    private void saveLanguage(int language_id, String language_name, String language_shortname) {
        ArrayList<Language> saved_languages = new ArrayList<>();
        saved_languages.add(new Language(language_id, language_name, language_shortname));
        utils.savePrefLanguage(saved_languages);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                language_id = data.getIntExtra("language_id", 0);
                language_name = data.getStringExtra("language_name");
                language_shortname = data.getStringExtra("language_shortname");
                saveLanguage(language_id, language_name, language_shortname);
                changeLocale(language_shortname);
                //Toast.makeText(context,String.valueOf(language_id)+language_name+language_shortname,Toast.LENGTH_LONG).show();
                utils.saveLanguageState(1);
                checkAppState();
            }
            if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    private void startApp() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashActivity.this, CatalogActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void killApp() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                System.exit(0);
            }
        }, KILL_TIME_LENGTH);
    }

    private void cacheImage() {
        try {
            options = new DisplayImageOptions.Builder()
                    .showImageOnFail(R.drawable.ic_menu)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(options)
                    .memoryCacheSize(50 * 1024 * 1024)
                    .diskCacheSize(50 * 1024 * 1024)
                    .threadPoolSize(10)
                    .writeDebugLogs()
                    .build();
            ;

            imageLoader = ImageLoader.getInstance();
            imageLoader.init(config);


            // new cacheTask().execute();

            for (int i = 0; i < allPress.size(); i++) {

                final int a = i;
                imageLoader.loadImage((allPress.get(i)).getOriginal_image_url(), new SimpleImageLoadingListener() {


                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                        Log.e("CACHEPRESS FAILED" + String.valueOf(a), ((allSets.get(a)).getImage()));

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                        Log.e("CACHEPRESS COMPLETED" + String.valueOf(a), ((allSets.get(a)).getImage()));

                    }


                });
            }


            Intent mainIntent = new Intent(SplashActivity.this, CatalogActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();

           /* for (int k = 0; k < allNews.size(); k++) {
                //imageLoader.loadImage((allNews.get(k)).getOriginal_image_url(), new SimpleImageLoadingListener());
                imageLoader.loadImageSync(allNews.get(k).getOriginal_image_url());
            }*/

           /*for (int c = 0; c < allChannel.size(); c++) {
               ImageLoader.getInstance().loadImage((allChannel.get(c)).getImage_url(), new SimpleImageLoadingListener());
                ArrayList<Series> serieList = allChannel.get(c).getSeriesList();
                for (int s = 0; s < serieList.size(); s++) {
                    ImageLoader.getInstance().loadImage(serieList.get(s).getOriginal_image_url(), new SimpleImageLoadingListener());
                }
            }*/

           /* for (int i = 0; i < allPress.size(); i++) {
                ImageLoader.getInstance().loadImage((allPress.get(i)).getOriginal_image_url(), new SimpleImageLoadingListener());
                Log.e("CACHEPRESS " + String.valueOf(i), ((allSets.get(i)).getImage()));

            }*/
        } catch (Exception e) {
            Log.e("SPLASH", "Cache error");
        }
    }

    private void startAnimation(int resourceId, int animId, long duration) {

        Animation splashAnim = AnimationUtils.loadAnimation(context, animId);
        splashAnim.setDuration(duration);
        findViewById(resourceId).startAnimation(splashAnim);

    }


    private class getAllProducts extends AsyncTask<String, String, JSONArray> {

        JsonParser jParser;
        JSONObject productsItem, collectionsItem, setsItem, newsJsonItem, mediaItem, mediaObject;
        JSONArray productsJsonArray, collectionsJsonArray, setsJsonArray, newsArray, mediaJsonArray;


        @Override
        protected void onPreExecute() {
            loadingBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONArray doInBackground(String... params) {

            jParser = new JsonParser();
            collectionsJsonArray = jParser.getJSONArrayFromUrl(Constants.LANGUAGE_CUSTOM + language_id);

            Product product;
            Collection collection;
            Sets sets;

            try {

                for (int i = 0; i < collectionsJsonArray.length(); i++) {
                    collection = new Collection();
                    collectionsItem = (JSONObject) collectionsJsonArray.get(i);
                    collection.setId(collectionsItem.get("id").toString());
                    if (Integer.parseInt(collectionsItem.get("language_id").toString()) == language_id) {
                        collection.setTitle(collectionsItem.get("title").toString());
                        collection.setUrl(Constants.MISSING_URL + collectionsItem.get("image").toString());
                        allCollection.add(collection);
                        productsJsonArray = collectionsItem.getJSONArray("sets");

                        for (int j = 0; j < productsJsonArray.length(); j++) {

                            product = new Product();
                            productsItem = (JSONObject) productsJsonArray.get(j);
                            product.setId(productsItem.getString("id"));
                            product.setTitle(productsItem.getString("title"));
                            //product.setDetail(productsItem.getString("detail"));
                            product.setThumb_image_url(Constants.MISSING_URL + productsItem.getString("image"));
                            product.setOriginal_image_url(Constants.MISSING_URL + productsItem.getString("image"));
                            product.setCollection_id(productsItem.getString("collection_id"));

                            allProduct.add(product);
                            setsJsonArray = productsItem.getJSONArray("products");

                            if (setsJsonArray != null) {
                                Log.e("setsJsonArray" + j, String.valueOf(setsJsonArray.length()));
                            }

                            for (int k = 0; k < setsJsonArray.length(); k++) {
                                sets = new Sets();
                                setsItem = (JSONObject) setsJsonArray.get(k);
                                sets.setId(setsItem.getString("id"));
                                sets.setTitle(setsItem.getString("title"));
                                //sets.setDescription(setsItem.getString("detail"));
                                String value = setsItem.getString("image");
                                JSONArray array = new JSONArray(value);

                                if (array.length() > 0) {
                                    sets.setImage(Constants.MISSING_URL + String.valueOf(array.get(0)));
                                } else {

                                    sets.setImage(Constants.MISSING_URL + "uploads/556f083e34541.jpg");
                                }
                                sets.setSet_id(setsItem.getString("set_id"));

                                Log.e("SETSINDEX", i + "--" + ((JSONObject) collectionsJsonArray.get(i)).getString("title") +
                                        "//" + j + "--" + ((JSONObject) productsJsonArray.get(j)).getString("title") +
                                        "//" + k + "--" + ((JSONObject) setsJsonArray.get(k)).getString("title"));

                                allSets.add(sets);
                            }
                        }
                    }
                }

                //GET NEWS OBJECTS
                newsArray = jParser.getJSONArrayFromUrl(Constants.NEWS_URL);
                News newsItem;


                for (int i = 0; i < newsArray.length(); i++) {
                    newsItem = new News();
                    newsJsonItem = (JSONObject) newsArray.get(i);
                    newsItem.setId(Integer.parseInt(newsJsonItem.get("id").toString()));
                    newsItem.setTitle(newsJsonItem.get("title").toString());
                    newsItem.setDetail(newsJsonItem.get("detail").toString());
                    if (Integer.parseInt(newsJsonItem.get("is_news").toString()) == 0) {
                        newsItem.setIs_news(false);

                    } else if (Integer.parseInt(newsJsonItem.get("is_news").toString()) == 1) {
                        newsItem.setIs_news(true);
                    }

                    String value = newsJsonItem.get("original_image_url").toString();
                    JSONArray array = new JSONArray(value);

                    if (array.length() > 0) {
                        newsItem.setThumb_image_url(Constants.MISSING_URL + String.valueOf(array.get(0)));
                        newsItem.setOriginal_image_url(Constants.MISSING_URL + String.valueOf(array.get(0)));

                    }

                    newsItem.setCreated_at(newsJsonItem.get("created_at").toString());
                    allNews.add(newsItem);
                }
                //GET MEDIA OBJECTS

                mediaObject = jParser.getJSONObjectFromUrl(Constants.MEDIA_URL);

                try {
                    mediaJsonArray = ((JSONArray) mediaObject.get("channels"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Channel channelItem;
                JSONArray jsonSeries;
                ArrayList<Series> seriesArrayList;
                ArrayList<String> series_images,image_details;

                for (int i = 0; i < mediaJsonArray.length(); i++) {

                    channelItem = new Channel();
                    mediaItem = (JSONObject) mediaJsonArray.get(i);
                    channelItem.setId(Integer.parseInt(mediaItem.get("id").toString()));
                    channelItem.setName(mediaItem.get("name").toString());
                    channelItem.setImage_url(Constants.MISSING_URL + mediaItem.get("image_url").toString());
                    jsonSeries = (JSONArray) mediaItem.get("series");

                    Series series;
                    JSONObject seriesObject;
                    seriesArrayList = new ArrayList<>();
                    for (int j = 0; j < jsonSeries.length(); j++) {
                        series = new Series();
                        series_images = new ArrayList<>();
                        image_details = new ArrayList<>();
                        seriesObject = (JSONObject) jsonSeries.get(j);
                        series.setId(Integer.parseInt(seriesObject.get("id").toString()));
                        series.setName(seriesObject.get("name").toString());
                        String value = seriesObject.get("original_image_url").toString();
                        String detail_value = seriesObject.get("text").toString();
                        series.setJsonImages(value);
                        series.setImage_details(detail_value);

                        JSONArray mediaArray = new JSONArray(value);

                        if (mediaArray.length() > 0) {
                            series.setThumb_image_url(Constants.MISSING_URL + String.valueOf(mediaArray.get(0)));
                            series.setOriginal_image_url(Constants.MISSING_URL + String.valueOf(mediaArray.get(0)));
                        }
                        series.setDescription(seriesObject.get("detail").toString());
                        series.setChannel_id(Integer.parseInt(seriesObject.get("channel_id").toString()));
                        seriesArrayList.add(series);

                    }

                    channelItem.setSeriesList(seriesArrayList);
                    allChannel.add(channelItem);
                }
                JSONArray pressArray;
                JSONObject jsonPress;
                pressArray = ((JSONArray) mediaObject.get("press"));
                News pressItem;


                for (int i = 0; i < pressArray.length(); i++) {

                    pressItem = new News();
                    jsonPress = (JSONObject) pressArray.get(i);
                    pressItem.setId(Integer.parseInt(jsonPress.get("id").toString()));
                    pressItem.setTitle(jsonPress.get("title").toString());
                    pressItem.setDetail(jsonPress.get("detail").toString());
                    if (Integer.parseInt(jsonPress.get("is_news").toString()) == 0) {
                        pressItem.setIs_news(false);

                    } else if (Integer.parseInt(jsonPress.get("is_news").toString()) == 1) {
                        pressItem.setIs_news(true);
                    }
                    String value = jsonPress.get("original_image_url").toString();
                    pressItem.setOriginal_image_url(Constants.MISSING_URL + value);
                    pressItem.setThumb_image_url(Constants.MISSING_URL + value);
                    pressItem.setCreated_at(jsonPress.get("created_at").toString());

                    allPress.add(pressItem);
                }


            } catch (Exception e) {
                Log.e("ASYNCERRoR", "GETALLASYNC");

            }
            return collectionsJsonArray;

        }

        @Override
        protected void onPostExecute(JSONArray jsonArray) {

            //new cacheTask().execute();
            //cacheImage();
            SharedPrefUtils utils = new SharedPrefUtils(context);
            utils.savePrefProduct(allProduct);
            utils.savePrefSet(allSets);
            utils.savePrefCollection(allCollection);
            Log.e("COLLECTION", String.valueOf(allCollection.get(0).getTitle()));
            utils.savePrefNews(allNews);
            utils.savePrefPress(allPress);
            utils.savePrefChannel(allChannel);
            loadingBar.setVisibility(View.GONE);

            Intent mainIntent = new Intent(SplashActivity.this, CatalogActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();
        }


    }

    private class cacheTask extends AsyncTask<String, String, JSONObject> {


        @Override
        protected void onPreExecute() {
        }

        @Override
        protected JSONObject doInBackground(String... params) {


            try {
                options = new DisplayImageOptions.Builder()
                        .showImageOnFail(R.drawable.ic_menu)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                        .build();

                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                        .defaultDisplayImageOptions(options)
                        .memoryCacheSize(50 * 1024 * 1024)
                        .diskCacheSize(50 * 1024 * 1024)
                        .threadPoolSize(5)
                        .writeDebugLogs()
                        .build();

                imageLoader = ImageLoader.getInstance();
                imageLoader.init(config);


                /*for (int c = 0; c < allChannel.size(); c++) {
                    ImageLoader.getInstance().loadImage((allChannel.get(c)).getImage_url(), new SimpleImageLoadingListener());
                    ArrayList<Series> serieList = allChannel.get(c).getSeriesList();
                    for (int s = 0; s < serieList.size(); s++) {
                        ImageLoader.getInstance().loadImage(serieList.get(s).getOriginal_image_url(), new SimpleImageLoadingListener());
                    }
                }*/


                for (int i = 0; i < allPress.size(); i++) {

                    final int a = i;
                    ImageLoader.getInstance().loadImage((allPress.get(i)).getOriginal_image_url(), new SimpleImageLoadingListener() {


                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                            Log.e("CACHEPRESS FAILED" + String.valueOf(a), ((allSets.get(a)).getImage()));

                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                            Log.e("CACHEPRESS COMPLETED" + String.valueOf(a), ((allSets.get(a)).getImage()));

                        }


                    });


                }
            } catch (Exception e) {
            }
            return null;

        }


        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            Intent mainIntent = new Intent(SplashActivity.this, CatalogActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();
            //Toast.makeText(context, "Tüm veriler Güncellendi", Toast.LENGTH_LONG).show();

        }
    }


}