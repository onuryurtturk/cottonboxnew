package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;

import batunet.cottonboxnav.Catalog;
import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Pages;

/**
 * Created by onuryurtturk on 4/22/2015.
 */
public class FavoriteGridAdapter extends ArrayAdapter {

    private Context context;
    DisplayImageOptions options;
    private ImageLoader imageLoader;
    private ArrayList<Pages> favArray;
    BitmapDrawable drawable;
    Bitmap share_bitmap;
    File file;
    ImageView catImage;

    public FavoriteGridAdapter(Context context, ArrayList<Pages> favorites) {
        super(context, 0);
        this.context = context;
        this.favArray = favorites;

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_no_photo)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        if (favArray == null) {
            favArray = new ArrayList<Pages>();
            Pages page = new Pages();
            page.setCatalog_id("Henüz Favori Eklemediniz!");
            page.setPhoto_file_name("Henüz Favori Eklemediniz!");
            page.setPhoto_original_url("http://www.aliagaekspres.com.tr/static/themes/default/no_photo.png?v=88621");
            favArray.add(page);

        }
    }


    @Override
    public int getCount() {
        return favArray.size();
    }

    @Override
    public Object getItem(int position) {
        return favArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private String findCatalog(String catId) {
        ArrayList<Catalog> savedCatalogs;
        String catalogsJSONString = context.getSharedPreferences("catalog", context.MODE_PRIVATE).getString("catalogs", null);
        Type type = new TypeToken<ArrayList<Catalog>>() {
        }.getType();
        savedCatalogs = new Gson().fromJson(catalogsJSONString, type);

        for (int i = 0; i < savedCatalogs.size(); i++) {
            if ((savedCatalogs.get(i)).getId().equals(catId)) ;
            {
                return (savedCatalogs.get(i)).getTitle();
            }

        }
        return "Test";
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View root = convertView;

        if (root == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater == null) {
                Log.e("INFLATER HATASI", "HATA");
            }
            root = inflater.inflate(R.layout.item_favorite, parent, false);
        }

        Pages page = favArray.get(position);


        TextView catTitle = (TextView) root.findViewById(R.id.fav_title);
        //ImageView secondView = (ImageView)root.findViewById(R.id.second_image);

        catTitle.setText(findCatalog(page.getCatalog_id()) + position);

        catImage = (ImageView) root.findViewById(R.id.fav_image);


        //imageLoader.displayImage(page.getPhoto_original_url(), catImage);


        String uri = (page.getPhoto_original_url()).toString();
        String decodedUri = Uri.decode(uri);

        final View failView = root;

        imageLoader.displayImage("file://" + decodedUri, catImage, options, new SimpleImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        message = "Lütfen İnternet Bağlantınızı Kontrol edin!";
                        catImage.setImageDrawable(failView.getResources().getDrawable(R.drawable.ic_no_photo));
                        break;
                    case DECODING_ERROR:
                        message = "Decode Edilemedi";
                        break;
                    case NETWORK_DENIED:
                        message = "İndirme Reddedildi";
                        break;
                    case OUT_OF_MEMORY:
                        message = "Bellek Dolu";
                        break;
                    case UNKNOWN:
                        message = "Bilinmeyen Bir Hata Oluştu";
                        break;
                }
                //Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {

            }
        });


        //file=imageLoader.getDiskCache().get(page.getPhoto_original_url());
        //secondView.setImageURI(Uri.fromFile(file));
        //drawable = (BitmapDrawable)secondView.getDrawable();
        //secondView.setImageBitmap(share_bitmap);


        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* drawable = (BitmapDrawable)catImage.getDrawable();
                share_bitmap = drawable.getBitmap();*/
                /*Intent intent = new Intent(context, FavActivity.class);
                intent.putExtra("fav_image", ((Pages) getItem(position)).getPhoto_original_url());
                intent.putExtra("share_bitmap", share_bitmap);
                context.startActivity(intent);*/
            }
        });


        return root;
    }
}
