package batunet.cottonboxnav.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.DrawerItemModel;

/**
 * Created by onuryurtturk on 3/23/2015.
 */
public class DrawerListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<DrawerItemModel> drawerList;
    // private Bitmap profileBbitmap;
    ExpandableListView exView;
    ArrayList<Object> tempChild;

    public DrawerListAdapter(Context context, ArrayList<DrawerItemModel> drawerList, ExpandableListView expandable) {
        this.context = context;
        this.drawerList = drawerList;
        this.exView = expandable;
    }

    @Override
    public int getGroupCount() {
        return drawerList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if ((((ArrayList<Object>) drawerList.get(groupPosition).getChilds()) != null)) {
            return (((ArrayList<Object>) drawerList.get(groupPosition).getChilds()).size());
        } else {
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_drawer_list, null);
        }

        exView.setGroupIndicator(null);

        ImageView itemImage = (ImageView) convertView.findViewById(R.id.item_image);
        TextView itemTitle = (TextView) convertView.findViewById(R.id.item_text);
        TextView itemCounter = (TextView) convertView.findViewById(R.id.item_counter);
        ImageView indicator = (ImageView) convertView.findViewById(R.id.item_arrow);

        if (drawerList.get(groupPosition).isVisible()) {
            itemCounter.setText(drawerList.get(groupPosition).getCount());
        } else {
            itemCounter.setVisibility(View.GONE);
        }


        itemImage.setImageResource(drawerList.get(groupPosition).getImage());
        itemTitle.setText(drawerList.get(groupPosition).getTitle());


        if ((((ArrayList<Object>) drawerList.get(groupPosition).getChilds())) == null) {

            indicator.setVisibility(View.GONE);
        }
        else
        {
            exView.expandGroup(groupPosition);
        }

        return convertView;


    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        if (drawerList.get(groupPosition).getChilds() != null) {
            tempChild = (ArrayList<Object>) drawerList.get(groupPosition).getChilds();
            LinearLayout.LayoutParams lpView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams lpimg = new LinearLayout.LayoutParams(20, 20);
            lpimg.gravity = Gravity.CENTER_VERTICAL;
            lpimg.setMargins(40, 20, 20, 20);


            TextView text = null;
            ImageView img = null;
            LinearLayout linearLayout = null;
            if (convertView == null) {
                convertView = new LinearLayout(context);
                text = new TextView(context);
                linearLayout = (LinearLayout) convertView;

                img = new ImageView(context);
                img.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_right));
                img.setLayoutParams(lpimg);
                linearLayout.addView(img, lpimg);

                text.setGravity(Gravity.LEFT);
                text.setPadding(5, 20, 20, 20);
                text.setTextSize(15);
                text.setLayoutParams(lpView);
                text.setTextColor(Color.BLACK);
                text.setText(tempChild.get(childPosition).toString());
                linearLayout.addView(text, lpView);

            }


            convertView.setTag(tempChild.get(childPosition));
            return convertView;
        }

        return null;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
