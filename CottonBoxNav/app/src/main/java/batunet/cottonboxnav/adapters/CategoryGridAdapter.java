package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Collection;

/**
 * Created by onuryurtturk on 5/14/2015.
 */
public class CategoryGridAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<Collection> collections;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private String collection_Id;
    AlertDialog.Builder builder;
    AlertDialog alert;
    private static int count = 0;



    public CategoryGridAdapter(Context context, ArrayList<Collection> collectionList) {
        super(context, 0);
        this.collections = collectionList;
        this.context = context;


        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                //.diskCacheSize(250 * 1024 * 1024)
                .diskCacheSize(410 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.custom_alert_dialog, null);
        builder = new AlertDialog.Builder(context);
        builder.setView(dialoglayout);
        builder.setCancelable(false);
        alert = builder.create();

    }


    @Override
    public int getCount() {
        return collections.size();
    }

    @Override
    public Object getItem(int position) {
        return collections.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View root, ViewGroup parent) {


        Holder holder = null;
        if (root == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            alert.show();

            if (inflater == null) {
                Log.e("INFLATER HATASI", "HATA");
            }
            root = inflater.inflate(R.layout.item_category, parent, false);

            holder = new Holder();

            holder.txt = (TextView) root.findViewById(R.id.category_title);
            holder.img = (ImageView) root.findViewById(R.id.category_image);
            root.setTag(holder);


        } else {
            holder = (Holder) root.getTag();
        }
        Collection collectionItem = collections.get(position);
        holder.txt.setText(collectionItem.getTitle());
        collection_Id = collectionItem.getId();

        final ProgressBar spinner = (ProgressBar) root.findViewById(R.id.loadingBar);

        System.out.println(position + "URL" + collectionItem.getId());
        imageLoader.displayImage(collectionItem.getUrl(), holder.img, options, new SimpleImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {

                spinner.setVisibility(View.VISIBLE);
                //alert.show();

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                Toast.makeText(context, view.getResources().getString(R.string.language_warning_net), Toast.LENGTH_LONG).show();
                spinner.setVisibility(View.VISIBLE);
                alert.dismiss();
                count++;

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                spinner.setVisibility(View.GONE);
                alert.dismiss();
                count++;


            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {

            }
        });

       if (count == collections.size()) {
            if (alert.isShowing()) {
                alert.dismiss();
            }
        }
        return root;
    }

    public class Holder {
        ImageView img;
        TextView txt;
    }
}
