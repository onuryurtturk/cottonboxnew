package batunet.cottonboxnav.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.util.ArrayList;

import batunet.cottonboxnav.ImageActivity;
import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.CatalogF;
import batunet.cottonboxnav.ECatalogActivity;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onuryurtturk on 4/18/2015.
 */
public class CatalogPagerAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<CatalogF> catalogList;
    LayoutInflater inflater;

    DisplayImageOptions options;
    private ImageLoader imageLoader;
    AlertDialog.Builder alert;
    SharedPrefUtils utils;
    ProgressBar spinner;
    AlertDialog alertDialog;
    Dialog d;


    public CatalogPagerAdapter(Context context, ArrayList<CatalogF> catalogList) {
        this.context = context;
        this.catalogList = catalogList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .diskCacheSize(250*1024*1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        //imageLoader.init(config);
        alert = new AlertDialog.Builder(context);
        utils = new SharedPrefUtils(context);

    }



    @Override
    public int getCount() {
        return catalogList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == ((LinearLayout) o);
    }

    @Override
    public float getPageWidth(int position) {
        return 1f;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View item = inflater.inflate(R.layout.item_catalog, container, false);

        final CatalogF catalog  = catalogList.get(position);

        ImageView thumb = (ImageView) item.findViewById(R.id.cat_image);

        String uri = (catalog.getCover_page());
        String decodedUri = Uri.decode(uri);
        String fulldecoded = "file://" +decodedUri;
        spinner = (ProgressBar) item.findViewById(R.id.cat_progress);

        imageLoader.displayImage(fulldecoded, thumb, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                Log.e("Hata","Start loading");
                spinner.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                Log.e("Hata", "ERRORRFAILED");
                spinner.setVisibility(View.VISIBLE);

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                spinner.setVisibility(View.GONE);

            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
            }
        });



        if(!catalog.is_downloaded())
        {
            showAlert();
        }


     item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ECatalogActivity.class);
                intent.putExtra("catalog", catalog);
                intent.putExtra("pages_array", catalog.getPages());
                intent.putExtra("cat_name", catalog.getName());
                ((Activity)context).startActivityForResult(intent,1);

                //context.startActivity(intent);
            }
        });

        item.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if(catalog.is_downloaded())
                {


                    alert.setTitle(context.getResources().getString(R.string.catalog_delete));
                    alert.setMessage(context.getResources().getString(R.string.catalog_delete_warning));
                    alert.setCancelable(false);
                    alert.setPositiveButton(context.getResources().getString(R.string.map_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            deleteDir(new File(Environment.getExternalStorageDirectory()
                                    + "/Cotton/" + catalog.getName()));
                            catalog.setIs_downloaded(false);
                            utils.savePrefCatalog(catalogList);



                        }
                    });
                    alert.setNegativeButton(context.getResources().getString(R.string.cache_cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    alert.show();

                }
                else
                {
                    Toast.makeText(context,context.getResources().getString(R.string.catalog_delete_fail),Toast.LENGTH_LONG).show();
                }

                return true;
            }
        });


        container.addView(item);
        return item;
    }


    private void showAlert()
    {
        /*View content = inflater.inflate(R.layout.custom_dialog, null);

        final AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(((ActionBarActivity)context), android.R.style.Theme_Black_NoTitleBar));
        dialog.setView(content);
        alertDialog = dialog.create();

       /* TextView dialogText = (TextView) content.findViewById(R.id.catalog_dialog_text);
        dialogText.setText(context.getResources().getString(R.string.catalog_dialog_text));
        Button butOk = (Button) content.findViewById(R.id.catalog_dialog_button);
        butOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });*/
        //alertDialog.show();
        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        View content = inflater.inflate(R.layout.dialog_catalog_warning, null);
        ImageView img_close = (ImageView)content.findViewById(R.id.img_close);
        d = adb.setView(content).create();



        /*WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(d.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;*/

        Window window = d.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(3));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.windowAnimations = R.anim.fade_in;
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        d.show();

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });


        //d.getWindow().setAttributes(lp);
    }


    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
