package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Contact;

/**
 * Created by onuryurtturk on 5/22/2015.
 */
public class ContactListAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<Contact> contacts;


    public ContactListAdapter(Context cntx,ArrayList<Contact> list) {
        super(cntx, 0);
        this.contacts = list;
        this.context = cntx;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View root = convertView;

        if (root == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater == null) {
                Log.e("INFLATER Problem", "ListAdapter");
            }

            if(!contacts.get(position).isGroupHeader()) {

                root = inflater.inflate(R.layout.item_contact, parent, false);

                TextView contactTitle = (TextView) root.findViewById(R.id.contact_text);
                contactTitle.setText(contacts.get(position).getTitle());

                ImageView contactImage = (ImageView) root.findViewById(R.id.contact_image);
                contactImage.setImageResource(contacts.get(position).getIcon());

            }
            else
            {
                root = inflater.inflate(R.layout.group_header_item, parent, false);
                TextView contactTitle = (TextView) root.findViewById(R.id.header);
                contactTitle.setText(contacts.get(position).getTitle());

            }

        }
        return root;
    }
}
