package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.utils.TouchImageView;

/**
 * Created by onur on 13.7.2015.
 */
public class SeriesImageAdapter extends PagerAdapter {

    private Context context;
    LayoutInflater inflater;
    ArrayList<String> series_images,series_details;
    private ImageLoader imageLoader;
    //private String description;
    private DisplayImageOptions options;


    public SeriesImageAdapter(Context context, ArrayList<String> sliderArray,ArrayList<String> series_text) {
        this.context = context;
        this.series_images = sliderArray;
        //this.description = series_text;
        this.series_details = series_text;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


    }

    @Override
    public int getCount() {
        return series_images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View item = inflater.inflate(R.layout.item_series_slider, container, false);
        ImageView sliderImage = (TouchImageView) item.findViewById(R.id.series_slider_image);
        TextView sliderText = (TextView) item.findViewById(R.id.series_slider_text);
        if (series_details!= null && series_details.size()>0) {
            sliderText.setText(series_details.get(position));
        }


        imageLoader.displayImage(series_images.get(position), sliderImage, options, new SimpleImageLoadingListener(), new ImageLoadingProgressListener() {

            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {

            }
        });

        container.addView(item, 0);
        return item;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
