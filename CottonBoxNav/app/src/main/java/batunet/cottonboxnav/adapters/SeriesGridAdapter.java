package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Series;

/**
 * Created by onuryurtturk on 5/21/2015.
 */
public class SeriesGridAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<Series> series;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    Holder holder;
    Series seriesItem;

    public SeriesGridAdapter(Context context, ArrayList<Series> seriesList) {
        super(context, 0);
        this.series = seriesList;
        this.context = context;


        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

    }


    @Override
    public int getCount() {
        return series.size();
    }

    @Override
    public Object getItem(int position) {
        return series.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View root = convertView;

        if (root == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            root = inflater.inflate(R.layout.item_press, parent, false);
            holder = new Holder();
            holder.image = (ImageView) root.findViewById(R.id.press_image);
            holder.title = (TextView) root.findViewById(R.id.press_title);

            root.setTag(holder);

        } else {
            holder = (Holder) root.getTag();
        }

        seriesItem = series.get(position);
        holder.title.setText(seriesItem.getName());
        holder.title.setTextColor(Color.BLACK);

        if (seriesItem.getThumb_image_url().equals("[]")) {
            holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.img_cotton_small));

        } else {

            imageLoader.displayImage(seriesItem.getThumb_image_url(), holder.image, options, new SimpleImageLoadingListener() {

                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    Log.e("Loading", "Name " + holder.title.getText() + " " + seriesItem.getThumb_image_url());
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                    holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.img_cotton_small));
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            }, new ImageLoadingProgressListener() {
                @Override
                public void onProgressUpdate(String imageUri, View view, int current, int total) {

                }
            });
        }


        return root;
    }

    public class Holder {
        ImageView image;
        TextView title;
    }

}
