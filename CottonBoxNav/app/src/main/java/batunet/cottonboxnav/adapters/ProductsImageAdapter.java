package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Product;
import batunet.cottonboxnav.models.Sets;
import batunet.cottonboxnav.utils.TouchImageView;

/**
 * Created by onur on 26.7.2015.
 */
public class ProductsImageAdapter extends PagerAdapter {

    private Context context;
    LayoutInflater inflater;
    ArrayList<Sets> sets_image;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;


    public ProductsImageAdapter(Context context, ArrayList<Sets> sliderArray) {
        this.context = context;
        this.sets_image = sliderArray;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


    }

    @Override
    public int getCount() {
        return sets_image.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View item = inflater.inflate(R.layout.item_product_slider, container, false);
        ImageView sliderImage = (TouchImageView) item.findViewById(R.id.product_slider_image);
        TextView sliderText = (TextView) item.findViewById(R.id.product_slider_text);
        sliderText.setText(sets_image.get(position).getTitle());

        imageLoader.displayImage(sets_image.get(position).getImage(), sliderImage, options, new SimpleImageLoadingListener(), new ImageLoadingProgressListener() {

            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {

            }
        });

        container.addView(item, 0);
        return item;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}