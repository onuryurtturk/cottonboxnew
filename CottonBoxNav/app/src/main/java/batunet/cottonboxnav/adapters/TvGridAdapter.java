package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Channel;

/**
 * Created by onuryurtturk on 5/18/2015.
 */
public class TvGridAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<Channel> channelList;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    Holder holder;


    public TvGridAdapter(Context context, ArrayList<Channel> channels) {
        super(context, 0);
        this.channelList = channels;
        this.context = context;


        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

    }


    @Override
    public int getCount() {
        return channelList.size();
    }

    @Override
    public Object getItem(int position) {
        return channelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        holder = null;
        Channel channelItem = channelList.get(position);
        View root = convertView;

        if (root == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater == null) {
                Log.e("INFLATER Problem", "Exception");
            }
            root = inflater.inflate(R.layout.item_tv, parent, false);


            holder = new Holder();

            holder.txt = (TextView) root.findViewById(R.id.tv_text);
            holder.img = (ImageView) root.findViewById(R.id.tv_image);
            root.setTag(holder);
        } else {
            holder = (Holder) root.getTag();
        }


        holder.txt.setText(channelItem.getName());

        imageLoader.displayImage(channelItem.getImage_url(), holder.img, options, new SimpleImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                Toast.makeText(context, "Internet Bağlantınızı Kontrol Edin!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {


            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {

            }
        });

        return root;
    }

    public class Holder {
        ImageView img;
        TextView txt;
    }
}
