package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import batunet.cottonboxnav.NewsDetailActivity;
import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.News;

/**
 * Created by onuryurtturk on 5/15/2015.
 */
public class NewsGridAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<News> newsArray;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    News newsItem;

    public NewsGridAdapter(Context context, ArrayList<News> newsList) {
        super(context, 0);
        this.newsArray = newsList;
        this.context = context;


        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .diskCacheSize(150 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


    }


    @Override
    public int getCount() {
        return newsArray.size();
    }

    @Override
    public Object getItem(int position) {
        return newsArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View root = convertView;
        Holder holder = null;

        if (root == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater == null) {
                Log.e("INFLATER ERROR", "ERROR");
            }
            //newsItem = newsArray.get(position);
            /*Log.e("CHECK", "THIS IS NEWS" + String.valueOf(newsItem.getId() + "/" + newsItem.getTitle() + "/" + newsItem.getDetail() + "/" + newsItem.getIs_news()));
            Log.e("CHECK", "THIS IS NEWS" + String.valueOf(newsItem.getIs_news()));*/
            root = inflater.inflate(R.layout.item_news, parent, false);
            holder = new Holder();
            holder.txt = (TextView) root.findViewById(R.id.news_title);
            holder.img = (ImageView) root.findViewById(R.id.news_icon);
            holder.date = (TextView) root.findViewById(R.id.news_date);
            root.setTag(holder);


        }
        else
        {
            holder = (Holder) root.getTag();

        }

        final News newsItem = newsArray.get(position);
        holder.txt.setText(newsItem.getTitle());
        holder.date.setText(newsItem.getCreated_at());
        holder.img.setImageDrawable(root.getResources().getDrawable(R.drawable.ic_news));


            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, NewsDetailActivity.class);
                    intent.putExtra("original_url", newsItem.getOriginal_image_url());
                    intent.putExtra("title_text",newsItem.getTitle());
                    intent.putExtra("detail_text", newsItem.getDetail());
                    context.startActivity(intent);

                }
            });


        return root;
    }

    public class Holder {
        ImageView img;
        TextView date;
        TextView txt;
    }


}

