package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Product;

/**
 * Created by onuryurtturk on 5/15/2015.
 */
public class ProductsGridAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<Product> products;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public ProductsGridAdapter(Context context, ArrayList<Product> productsList) {
        super(context, 0);
        this.products = productsList;
        this.context = context;


        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .resetViewBeforeLoading(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCache(new WeakMemoryCache())
                .diskCacheSize(250 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

    }


    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View root = convertView;
        Holder holder = null;

        if (root == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater == null) {
                Log.e("INFLATER HATASI", "HATA");
            }
            root = inflater.inflate(R.layout.item_product, parent, false);
            holder = new Holder();
            holder.txt = (TextView) root.findViewById(R.id.product_title);
            holder.img = (ImageView) root.findViewById(R.id.product_image);
            root.setTag(holder);
        }
        else
        {
            holder = (Holder) root.getTag();

        }


        final Product productItem = products.get(position);

        holder.txt.setText(productItem.getTitle());


        final ProgressBar spinner = (ProgressBar) root.findViewById(R.id.loadingBar);

        imageLoader.displayImage(productItem.getThumb_image_url(), holder.img, options, new SimpleImageLoadingListener() {

                @Override
                public void onLoadingStarted(String imageUri, View view) {

                    spinner.setVisibility(View.VISIBLE);


                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                    Toast.makeText(context, context.getResources().getString(R.string.language_warning_net), Toast.LENGTH_LONG).show();
                    spinner.setVisibility(View.VISIBLE);


                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                    spinner.setVisibility(View.GONE);


                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {


                }
            }, new ImageLoadingProgressListener() {
                @Override
                public void onProgressUpdate(String imageUri, View view, int current, int total) {

                }
            });


        return root;
    }


    public class Holder {
        ImageView img;
        TextView txt;
    }
}

