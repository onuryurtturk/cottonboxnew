package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Slider;

/**
 * Created by onuryurtturk on 5/14/2015.
 */
public class SliderAdapter extends PagerAdapter {

    private Context context;
    LayoutInflater inflater;
    ArrayList<Slider> sliderArray;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    TextView sliderText;
    ImageView sliderImage,sliderTemp;
    ProgressBar spinner;
    int currentPosition;

    public SliderAdapter(Context context, ArrayList<Slider> sliderArray) {
        this.context = context;
        this.sliderArray = sliderArray;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheExtraOptions(500, 500)
                .diskCacheExtraOptions(480, 320, null)
                .diskCacheSize(500 * 1024 * 1024)
                .threadPoolSize(7)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


    }

    @Override
    public int getCount() {
        return sliderArray.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View item = inflater.inflate(R.layout.item_slider, container, false);

        sliderImage = (ImageView) item.findViewById(R.id.slider_image);
        sliderTemp = (ImageView) item.findViewById(R.id.slider_temp_image);
        sliderText = (TextView) item.findViewById(R.id.slider_text);
        spinner = (ProgressBar) item.findViewById(R.id.loadingBar);

        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/aka.ttf");
        sliderText.setTypeface(face);

        currentPosition = position;
        imageLoader.displayImage(sliderArray.get(position).getUrl(), sliderImage, options, new SimpleImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {

                spinner.setVisibility(View.VISIBLE);

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                Toast.makeText(context, "Internet Bağlantınızı Kontrol Edin!", Toast.LENGTH_LONG).show();
                spinner.setVisibility(View.VISIBLE);

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                sliderTemp.setVisibility(View.GONE);
                spinner.setVisibility(View.GONE);
                if (sliderArray.get(currentPosition).getText() != null) {
                    sliderText.setText(sliderArray.get(currentPosition).getText());
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {

            }
        });

        container.addView(item, 0);
        return item;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
