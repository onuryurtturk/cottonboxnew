package batunet.cottonboxnav.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Home;
import batunet.cottonboxnav.models.Language;
import batunet.cottonboxnav.models.News;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 24.7.2015.
 */
public class HomePageAdapter extends ArrayAdapter {

    private Context context;
    private ArrayList<Home> homeArray;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    AlertDialog.Builder builder;
    AlertDialog alert;
    ProgressBar spinner;
    SharedPrefUtils utils;
    Language appLanguage;
    private static int count = 0;
    int runState,offline;


    public HomePageAdapter(Context context, ArrayList<Home> homes) {
        super(context, 0);
        this.homeArray = homes;
        this.context = context;

        utils = new SharedPrefUtils(context);
        appLanguage = ((ArrayList<Language>)utils.getPrefLanguage()).get(0);
        runState = utils.getFirstRun();
        offline = utils.getOfflineMode();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .diskCacheSize(200 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.custom_alert_dialog, null);
        builder = new AlertDialog.Builder(context);
        builder.setView(dialoglayout);
        builder.setCancelable(false);
        alert = builder.create();


    }


    @Override
    public int getCount() {
        return homeArray.size();
    }

    @Override
    public Object getItem(int position) {
        return homeArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View root = convertView;
        Holder holder = null;

        if (root == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater == null) {
                Log.e("INFLATER ERROR", "ERROR");
            }

            root = inflater.inflate(R.layout.item_home, parent, false);
            holder = new Holder();
            holder.txt_title = (TextView) root.findViewById(R.id.item_home_title);
            holder.txt_sub = (TextView) root.findViewById(R.id.item_home_sub);
            holder.txt_detail = (TextView) root.findViewById(R.id.item_home_detail);
            holder.img = (ImageView) root.findViewById(R.id.item_home_image);
            root.setTag(holder);
        }
        else{

            holder = (Holder) root.getTag();
        }

        final Home homeItem = homeArray.get(position);
        holder.txt_title.setText(homeItem.getTitle());
        holder.txt_sub.setText(homeItem.getSubTitle());
        holder.txt_detail.setText(homeItem.getContent());
        spinner = (ProgressBar) root.findViewById(R.id.loadingBar);

        if(appLanguage.getLanguage_short()!="ru" && appLanguage.getLanguage_short()!="ar")
        {
            Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/dosis.ttf");
            holder.txt_title.setTypeface(face,Typeface.BOLD);
            holder.txt_sub.setTypeface(face,Typeface.BOLD);
            holder.txt_detail.setTypeface(face);

        }

        imageLoader.displayImage(homeItem.getImage(), holder.img, options, new SimpleImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {

                /*spinner.setVisibility(View.VISIBLE);
                if(offline==1)
                {
                    alert.show();
                }
*/
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                Toast.makeText(context, view.getResources().getString(R.string.language_warning_net), Toast.LENGTH_LONG).show();
                spinner.setVisibility(View.VISIBLE);
                alert.dismiss();
                count++;

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

               spinner.setVisibility(View.GONE);
               alert.dismiss();
                count++;


            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {

            }
        });


        if (count == homeArray.size()) {
            if (alert.isShowing() && (runState==1 || offline==1)) {
                alert.dismiss();
            }
        }


        return root;
    }

    public class Holder {
        ImageView img;
        TextView txt_title,txt_sub,txt_detail;
    }


}
