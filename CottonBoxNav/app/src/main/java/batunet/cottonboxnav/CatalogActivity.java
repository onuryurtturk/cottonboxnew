package batunet.cottonboxnav;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import batunet.cottonboxnav.adapters.DrawerListAdapter;
import batunet.cottonboxnav.fragments.FragmentCatalog;
import batunet.cottonboxnav.fragments.FragmentCollection;
import batunet.cottonboxnav.fragments.FragmentContact;
import batunet.cottonboxnav.fragments.FragmentHomePage;
import batunet.cottonboxnav.fragments.FragmentMedia;
import batunet.cottonboxnav.fragments.FragmentNews;
import batunet.cottonboxnav.fragments.FragmentSettings;
import batunet.cottonboxnav.models.Channel;
import batunet.cottonboxnav.models.Collection;
import batunet.cottonboxnav.models.DrawerItemModel;
import batunet.cottonboxnav.models.Language;
import batunet.cottonboxnav.models.News;
import batunet.cottonboxnav.models.Product;
import batunet.cottonboxnav.models.Series;
import batunet.cottonboxnav.models.Sets;
import batunet.cottonboxnav.models.Slider;
import batunet.cottonboxnav.utils.ActivityResultBus;
import batunet.cottonboxnav.utils.ActivityResultEvent;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.OnSwipeTouchListener;
import batunet.cottonboxnav.utils.SharedPrefUtils;


public class CatalogActivity extends ActionBarActivity {

    Context context;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private CharSequence drawerTitle = "";
    private CharSequence appTitle = "";
    private ExpandableListView list;
    private String[] drawerTitles;
    private TypedArray drawerIcons;
    private ArrayList<DrawerItemModel> drawerList;
    private DrawerListAdapter drawerAdapter;
    private final Handler mDrawerHandler = new Handler();
    private ProgressDialog mDialog;
    public static ArrayList<Catalog> catalogList;
    SharedPreferences mPrefs, mFirst;
    ArrayList<String> groupItem;
    ArrayList<Object> childItem;
    public static ArrayList<Collection> collections;
    SharedPreferences prefSlider, prefCollection;
    public static ArrayList<Collection> allCollection;
    public static ArrayList<Product> allProduct;
    public static ArrayList<Sets> allSets;
    public static ArrayList<News> allNews, allPress;
    public static ArrayList<Channel> allChannel;
    public ArrayList<Language> languages;
    private ArrayList<Slider> sliderImages;
    NetworkOperations operations;
    android.support.v7.app.AlertDialog.Builder builder;
    android.support.v7.app.AlertDialog alert;
    SharedPrefUtils utils;
    Language appLanguage;
    TextView actionText;
    Dialog d;


    DisplayImageOptions options;
    private ImageLoader imageLoader;


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_menu);

        context = this;
        mFirst = getSharedPreferences("first", 0);
        mPrefs = getSharedPreferences("catalog", MODE_PRIVATE);
        catalogList = new ArrayList<Catalog>();
        groupItem = new ArrayList<String>();
        childItem = new ArrayList<Object>();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        groupItem.add(getResources().getString(R.string.menu_home));
        groupItem.add(getResources().getString(R.string.menu_collection));
        groupItem.add(getResources().getString(R.string.menu_news));
        groupItem.add(getResources().getString(R.string.menu_press));
        groupItem.add(getResources().getString(R.string.menu_tv));
        groupItem.add(getResources().getString(R.string.menu_catalog));
        groupItem.add(getResources().getString(R.string.menu_contact));
        groupItem.add(getResources().getString(R.string.menu_settings));

        childItem.add(getResources().getString(R.string.menu_collection_sub4));
        childItem.add(getResources().getString(R.string.menu_collection_sub2));
        childItem.add(getResources().getString(R.string.menu_collection_sub3));
        childItem.add(getResources().getString(R.string.menu_collection_sub1));

        drawerTitles = getResources().getStringArray(R.array.nav_bar_items);
        drawerIcons = getResources().obtainTypedArray(R.array.nav_bar_images);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        list = (ExpandableListView) findViewById(R.id.slider_list);
        drawerList = new ArrayList<DrawerItemModel>();

        appTitle = drawerTitle = getTitle();

        operations = new NetworkOperations(context);

        prefSlider = getSharedPreferences("slider", MODE_PRIVATE);
        prefCollection = getSharedPreferences("collection", MODE_PRIVATE);


        initArrayList(context);
        initActionBar();


        if (operations.isNetworkConnected() == true) {

            if (utils.getFirstRun() == 0) {
                showcaseAlert();
                utils.saveFirstRun(1);
                cacheAllImages();
            } else if (utils.getOfflineMode() == 1) {
                cacheAllImages();
            }
            new JsonSliderParse().execute(Constants.SLIDER_URL);

        } else {

            showAlert(context);
            //slider.setPageTransformer(true, new RotateUpTransformer());
            //slider.setPageTransformer(true, new RotateUpTransformer());
            disMissAlert();
        }
        DrawerItemModel model;
        for (int i = 0; i < groupItem.size(); i++) {
            model = new DrawerItemModel(groupItem.get(i), drawerIcons.getResourceId(i, -1));
            if (i == 1) {
                model.setChilds(childItem);
            }
            drawerList.add(model);
        }
        drawerIcons.recycle();


        drawerAdapter = new DrawerListAdapter(getApplicationContext(), drawerList, list);
        list.setAdapter(drawerAdapter);

        displayMain();
        list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                FragmentManager fm = getSupportFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                displayView(groupPosition);
                return false;
            }
        });

        list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                //Nothing here ever fires

                FragmentManager fm = getSupportFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                Collection item = collections.get(childPosition);
                Intent intent = new Intent(context, ProductsActivity.class);
                intent.putExtra("collection_id", item.getId());
                intent.putExtra("collection_name", item.getTitle());
                context.startActivity(intent);
                return true;
            }
        });


        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {

                //getSupportActionBar().setTitle(appTitle);
                actionText.setText("CottonBox");
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                //getSupportActionBar().setTitle(drawerTitle);
                //actionText.setText("OPENED");
                invalidateOptionsMenu();
            }
        };


        list.setOnItemClickListener(new drawerClickListener());


    }

    public void closeKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ActivityResultBus.getInstance().postQueue(new ActivityResultEvent(requestCode, resultCode, data));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (mDialog != null) {
            mDialog.dismiss();
            displayMain();
            mDialog = null;
        }
    }


    private android.support.v7.app.AlertDialog showAlert(Context context) {
        View dialoglayout = getLayoutInflater().inflate(R.layout.custom_alert_dialog, null);
        builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setView(dialoglayout);
        builder.setCancelable(false);
        alert = builder.create();
        alert.show();

        return alert;
    }

    private void disMissAlert() {

        if (alert != null) {
            alert.dismiss();
        }
    }

    private void initArrayList(Context context) {
        utils = new SharedPrefUtils(context);
        sliderImages = new ArrayList<>();

        //initialise arraylists
        languages = new ArrayList<>();
        sliderImages = new ArrayList();
        collections = new ArrayList<>();
        allCollection = new ArrayList<>();


        try {
            languages = utils.getPrefLanguage();
            allProduct = utils.getPrefProduct();
            allSets = utils.getPrefSet();
            allNews = utils.getPrefNews();
            allChannel = utils.getPrefChannel();
            allPress = utils.getPrefPress();
            collections = utils.getPrefCollection();
            appLanguage = languages.get(0);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("ARRAYLIST", "error");
        }

    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        View view = getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView) view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        //actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

    }

    private void showcaseAlert() {

        android.support.v7.app.AlertDialog.Builder adb = new android.support.v7.app.AlertDialog.Builder(context);
        View content = getLayoutInflater().inflate(R.layout.dialog_first_run, null);

        Button btn_first = (Button) content.findViewById(R.id.btn_first_run);
        RelativeLayout root_layout = (RelativeLayout) content.findViewById(R.id.first_dialog_root);
        View menu_layout = (View)content.findViewById(R.id.first_dialog_view);
        d = new Dialog(context, android.R.style.Theme_Light_NoTitleBar);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(content);
        Window window = d.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(3));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.windowAnimations = R.anim.fade_in;
        wlp.gravity = Gravity.CENTER_VERTICAL;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        d.show();

        menu_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });

        btn_first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        root_layout.setOnTouchListener(new OnSwipeTouchListener(context) {

            @Override
            public void onSwipeRight() {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
            @Override
            public void onSwipeLeft() {
                drawerLayout.closeDrawer(Gravity.LEFT);
            }
            @Override
            public void doubleTap() {
                d.dismiss();
            }

        });
    }
    private void cacheAllImages() {

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheSize(250 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();


        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


        new cacheMedia().execute();
        for (int j = 0; j < allProduct.size(); j++) {

            Log.e("CACHEPRODUCTS " + String.valueOf(j), (allProduct.get(j)).getOriginal_image_url());
            imageLoader.loadImage((allProduct.get(j)).getOriginal_image_url(), new SimpleImageLoadingListener());

        }

        for (int k = 0; k < allSets.size(); k++) {
            Log.e("CACHESETS " + String.valueOf(k), ((allSets.get(k)).getImage()));

            imageLoader.loadImage((allSets.get(k)).getImage(), new SimpleImageLoadingListener());

        }

    }

    private class drawerClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            drawerLayout.closeDrawer(list);
            displayView(position);
        }

    }

    private void displayMain() {
        Fragment fragment = new FragmentHomePage();
        Bundle bundleObject = new Bundle();
        bundleObject.putSerializable("catalogs", getPreferences());
        fragment.setArguments(bundleObject);

        if (fragment != null) {

            final Fragment frg = fragment;

            mDrawerHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    FragmentManager manager = getSupportFragmentManager();
                    manager.beginTransaction().replace(R.id.content_frame, frg).commit();
                }
            }, 300);

        } else {
            Log.e("Fragment Error", "Main Activity Check Fragment");
        }
    }

    private ArrayList<Catalog> getPreferences() {
        ArrayList<Catalog> savedCatalogs;
        String catalogsJSONString = context.getSharedPreferences("catalog", MODE_PRIVATE).getString("catalogs", null);
        Type type = new TypeToken<ArrayList<Catalog>>() {
        }.getType();
        savedCatalogs = new Gson().fromJson(catalogsJSONString, type);
        return savedCatalogs;
    }


    private void displayView(final int position) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        switch (position) {
            case 0:
                fragment = new FragmentHomePage();
                break;
            case 1:
                fragment = new FragmentCollection();
                break;
            case 2:
                fragment = new FragmentNews();
                break;
            case 3:
                fragment = new FragmentMedia();
                bundle.putInt("index", 1);
                fragment.setArguments(bundle);
                break;
            case 4:
                fragment = new FragmentMedia();
                bundle = new Bundle();
                bundle.putInt("index", 0);
                fragment.setArguments(bundle);
                break;
            case 5:
                fragment = new FragmentCatalog();
                break;
            case 6:
                fragment = new FragmentContact();
                break;
            case 7:
                fragment = new FragmentSettings();
                break;
            default:
                break;
        }


        if (fragment != null) {

            final Fragment frg = fragment;
            final int pos = position;
            mDrawerHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    FragmentManager manager = getSupportFragmentManager();
                    manager.beginTransaction().replace(R.id.content_frame, frg).commit();
                    list.setItemChecked(pos, true);
                    list.setSelection(pos);
                    drawerLayout.closeDrawer(list);

                }
            }, 300);
            //getSupportActionBar().setTitle(drawerTitles[position + 1]);
            //getSupportActionBar().setTitle(drawerTitles[position]);
            actionText.setText(drawerTitles[position]);


        } else {
            Log.e("Fragment Error", "Main Activity Check Fragment");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (toggle.onOptionsItemSelected(item)) {
            closeKeyboard();
            return true;
        }

        int id = item.getItemId();
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private class JsonSliderParse extends AsyncTask<String, String, JSONArray> {

        JsonParser jParser;
        JSONObject sliderItem;
        JSONArray sliderJsonArray;

        @Override
        protected void onPreExecute() {
            showAlert(context);
        }


        @Override
        protected JSONArray doInBackground(String... params) {

            jParser = new JsonParser();
            sliderJsonArray = jParser.getJSONArrayFromUrl(Constants.SLIDER_URL);
            Slider slider;

            try {
                for (int i = 0; i < sliderJsonArray.length(); i++) {
                    sliderItem = (JSONObject) sliderJsonArray.get(i);
                    slider = new Slider();
                    slider.setUrl(sliderItem.getString("url"));
                    sliderItem = (JSONObject) sliderItem.get("text");
                    slider.setText(sliderItem.getString(appLanguage.getLanguage_short()));
                    sliderImages.add(slider);
                }

            } catch (Exception e) {
                Log.e("ASNYCTASK", "DoInBackgroundError");
            }

            return sliderJsonArray;
        }


        @Override
        protected void onPostExecute(JSONArray jsonArray) {

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            utils.savePrefCollection(collections);
            utils.savePrefSlider(sliderImages);
            disMissAlert();
            /*JsonCategoryParse categoryTask = new JsonCategoryParse();
            categoryTask.execute(Constants.CATEGORY_URL);*/
        }

    }


    private class cacheMedia extends AsyncTask<String, String, JSONArray> {
        @Override
        protected JSONArray doInBackground(String... params) {


            for (int k = 0; k < allNews.size(); k++) {
                imageLoader.loadImage(allNews.get(k).getOriginal_image_url(), new SimpleImageLoadingListener());
                Log.e("NEWS " + String.valueOf(k), "Image Cached");
            }

            for (int col = 0; col < collections.size(); col++) {
                imageLoader.loadImage(collections.get(col).getUrl(), new SimpleImageLoadingListener());

            }


            return new JSONArray();
        }

        @Override
        protected void onPostExecute(JSONArray jsonArray) {


            for (int c = 0; c < allChannel.size(); c++) {
                imageLoader.loadImage((allChannel.get(c)).getImage_url(), new SimpleImageLoadingListener());
                ArrayList<Series> serieList = allChannel.get(c).getSeriesList();
                for (int s = 0; s < serieList.size(); s++) {
                    imageLoader.loadImage(serieList.get(s).getOriginal_image_url(), new SimpleImageLoadingListener());
                    //Log.e("CHANNEL", serieList.get(s).getName() + serieList.get(s).);


                    JSONArray mediaArray = null;
                    try {
                        mediaArray = new JSONArray(serieList.get(s).getJsonImages());

                        if (mediaArray.length() > 0) {
                            for (int m = 0; m < mediaArray.length(); m++) {

                                imageLoader.loadImage(Constants.MISSING_URL + String.valueOf(mediaArray.get(m)), new SimpleImageLoadingListener());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                   /* if (serieList.get(s).getImages() == null) {
                        Log.e("NULLNULL", "NULLL");
                    }
                    for (int m = 0; m < (serieList.get(s).getImages()).size(); m++) {
                        ImageLoader.getInstance().loadImage(serieList.get(s).getImages().get(m), new SimpleImageLoadingListener());
                    }*/

                }
            }

            new pressMedia().execute();

        }

    }


    private class pressMedia extends AsyncTask<String, String, JSONArray> {
        @Override
        protected JSONArray doInBackground(String... params) {

            for (int i = 0; i < allPress.size(); i++) {
                imageLoader.loadImage((allPress.get(i)).getOriginal_image_url(), new SimpleImageLoadingListener());
            }

            return new JSONArray();
        }

    }

}
