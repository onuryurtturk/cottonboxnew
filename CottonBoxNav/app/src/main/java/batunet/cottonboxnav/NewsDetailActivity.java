package batunet.cottonboxnav;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import batunet.cottonboxnav.utils.TouchImageView;

/**
 * Created by onuryurtturk on 5/15/2015.
 */
public class NewsDetailActivity extends ActionBarActivity {


    Context context;
    private static String original_url;
    private static String detail_text;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageView newsView;
    private TextView newsText, actionText;
    private String title_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_news_detail);
        context = this;
        original_url = getIntent().getStringExtra("original_url");
        detail_text = getIntent().getStringExtra("detail_text");
        title_text = getIntent().getStringExtra("title_text");

        newsView = (TouchImageView) findViewById(R.id.news_detail_image);
        newsText = (TextView) findViewById(R.id.news_detail_text);
        newsText.setText(detail_text);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        imageLoader.displayImage(original_url, newsView, options,
                new SimpleImageLoadingListener() {
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current, int total) {

                    }
                });
        initActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        View view = getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView) view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(title_text);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}