package batunet.cottonboxnav.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;

import batunet.cottonboxnav.models.CatalogF;
import batunet.cottonboxnav.models.Channel;
import batunet.cottonboxnav.models.Collection;
import batunet.cottonboxnav.models.Home;
import batunet.cottonboxnav.models.Language;
import batunet.cottonboxnav.models.News;
import batunet.cottonboxnav.models.Product;
import batunet.cottonboxnav.models.Sets;
import batunet.cottonboxnav.models.Slider;

/**
 * Created by onur on 15.6.2015.
 */
public class SharedPrefUtils {


    Context context;
    SharedPreferences preferences;

    public SharedPrefUtils(Context context) {
        this.context = context;
    }


    public void saveOfflineMode(int value)
    {
        preferences = context.getSharedPreferences("offline", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("offline");
        prefsEditor.putInt("offline", value);
        prefsEditor.commit();
    }
    public int getOfflineMode()
    {
        preferences = context.getSharedPreferences("offline", Context.MODE_PRIVATE);
        int value =preferences.getInt("offline", 0);
        return value;
    }


    public void saveFirstRun(int value)
    {
        preferences = context.getSharedPreferences("first", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("first");
        prefsEditor.putInt("first", value);
        prefsEditor.commit();
    }
    public int getFirstRun()
    {
        preferences = context.getSharedPreferences("first", Context.MODE_PRIVATE);
        int value =preferences.getInt("first", 0);
        return value;
    }




    public void saveLanguageState(int value)
    {
        preferences = context.getSharedPreferences("slanguage", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("slanguage");
        prefsEditor.putInt("slanguage", value);
        prefsEditor.commit();
    }
    public int getLanguageState()
    {
        preferences = context.getSharedPreferences("slanguage", Context.MODE_PRIVATE);
        int value =preferences.getInt("slanguage", 0);
        return value;
    }

    public void savePrefHome(ArrayList<Home> homes) {
        preferences = context.getSharedPreferences("home", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("home");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(homes);
        prefsEditor.putString("home", json);
        prefsEditor.commit();

    }

    public ArrayList<Home> getPrefHome() {
        preferences = context.getSharedPreferences("home", Context.MODE_PRIVATE);
        ArrayList<Home> savedHome;
        String productsJSONString = context.getSharedPreferences("home", Context.MODE_PRIVATE).getString("home", null);
        Type type = new TypeToken<ArrayList<Home>>() {
        }.getType();
        savedHome = new Gson().fromJson(productsJSONString, type);
        return savedHome;
    }



    public void savePrefCatalog(ArrayList<CatalogF> catalogs) {
        preferences = context.getSharedPreferences("catalogs", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("catalogs");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(catalogs);
        prefsEditor.putString("catalogs", json);
        prefsEditor.commit();

    }

    public ArrayList getPrefCatalog() {
        preferences = context.getSharedPreferences("catalogs", Context.MODE_PRIVATE);
        ArrayList<CatalogF> savedCatalog;
        String catalogsJSONString = context.getSharedPreferences("catalogs", Context.MODE_PRIVATE).getString("catalogs", null);
        Type type = new TypeToken<ArrayList<CatalogF>>() {
        }.getType();
        savedCatalog = new Gson().fromJson(catalogsJSONString, type);
        return savedCatalog;
    }



    public void savePrefSlider(ArrayList<Slider> sliderImages) {
        preferences = context.getSharedPreferences("slider", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("slider");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(sliderImages);
        prefsEditor.putString("slider", json);
        prefsEditor.commit();

    }

    public ArrayList getPrefSlider() {
        preferences = context.getSharedPreferences("slider", Context.MODE_PRIVATE);
        ArrayList<Slider> savedSlider;
        String productsJSONString = context.getSharedPreferences("slider", Context.MODE_PRIVATE).getString("slider", null);
        Type type = new TypeToken<ArrayList<Slider>>() {
        }.getType();
        savedSlider = new Gson().fromJson(productsJSONString, type);
        return savedSlider;
    }

    public void savePrefCacheState(int value) {
        preferences = context.getSharedPreferences("state", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("state");
        prefsEditor.clear().commit();
        prefsEditor.putInt("state", value);
        prefsEditor.commit();
    }

    public int getPrefCacheState() {
        preferences = context.getSharedPreferences("state", Context.MODE_PRIVATE);
        int state = preferences.getInt("state", 0);

        return state;
    }

    public void savePrefLanguage(ArrayList<Language> languages)
    {
        preferences = context.getSharedPreferences("language", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("language");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(languages);
        prefsEditor.putString("language", json);
        prefsEditor.commit();


    }
    public ArrayList<Language> getPrefLanguage()
    {
        preferences = context.getSharedPreferences("language", Context.MODE_PRIVATE);
        ArrayList<Language> savedLanguages;
        String languagesJSONString = context.getSharedPreferences("language", Context.MODE_PRIVATE).getString("language", null);
        Type type = new TypeToken<ArrayList<Language>>() {}.getType();
        savedLanguages = new Gson().fromJson(languagesJSONString, type);
        return savedLanguages;
    }


    public void savePrefProduct(ArrayList<Product> products) {
        preferences = context.getSharedPreferences("product", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("product");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(products);
        prefsEditor.putString("product", json);
        prefsEditor.commit();
    }

    public ArrayList<Product> getPrefProduct() {
        preferences = context.getSharedPreferences("product", Context.MODE_PRIVATE);
        ArrayList<Product> savedProducts;
        String productsJSONString = context.getSharedPreferences("product", Context.MODE_PRIVATE).getString("product", null);
        Type type = new TypeToken<ArrayList<Product>>() {
        }.getType();
        savedProducts = new Gson().fromJson(productsJSONString, type);
        return savedProducts;
    }

    public void savePrefSet(ArrayList<Sets> sets) {
        preferences = context.getSharedPreferences("set", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("set");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(sets);
        prefsEditor.putString("set", json);
        prefsEditor.commit();

    }

    public ArrayList<Sets> getPrefSet() {
        preferences = context.getSharedPreferences("set", Context.MODE_PRIVATE);
        ArrayList<Sets> savedSets;
        String productsJSONString = context.getSharedPreferences("set", Context.MODE_PRIVATE).getString("set", null);
        Type type = new TypeToken<ArrayList<Sets>>() {
        }.getType();
        savedSets = new Gson().fromJson(productsJSONString, type);
        return savedSets;
    }


    public void savePrefChannel(ArrayList<Channel> channels) {
        preferences = context.getSharedPreferences("channel", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("channel");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(channels);
        prefsEditor.putString("channel", json);
        prefsEditor.commit();
    }

    public ArrayList<Channel> getPrefChannel() throws JSONException {
        preferences = context.getSharedPreferences("channel", Context.MODE_PRIVATE);
        ArrayList<Channel> savedChannels = new ArrayList<>();
        String channelsJSONString = context.getSharedPreferences("channel", Context.MODE_PRIVATE).getString("channel", null);
        Log.e("CHANNEL", channelsJSONString);
        Type type = new TypeToken<ArrayList<Channel>>() {
        }.getType();
        savedChannels = new Gson().fromJson(channelsJSONString, type);

        return savedChannels;
    }


    public void savePrefPress(ArrayList<News> press) {
        preferences = context.getSharedPreferences("press", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("press");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(press);
        prefsEditor.putString("press", json);
        prefsEditor.commit();
    }

    public ArrayList<News> getPrefPress() {
        preferences = context.getSharedPreferences("press", Context.MODE_PRIVATE);
        ArrayList<News> savedNews;
        String newsJSONString = context.getSharedPreferences("press", Context.MODE_PRIVATE).getString("press", null);
        Type type = new TypeToken<ArrayList<News>>() {
        }.getType();
        savedNews = new Gson().fromJson(newsJSONString, type);
        return savedNews;
    }

    public void savePrefNews(ArrayList<News> news) {
        preferences = context.getSharedPreferences("news", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("news");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(news);
        prefsEditor.putString("news", json);
        prefsEditor.commit();
    }


    public ArrayList<News> getPrefNews() {
        preferences = context.getSharedPreferences("news", Context.MODE_PRIVATE);
        ArrayList<News> savedNews;
        String newsJSONString = context.getSharedPreferences("news", Context.MODE_PRIVATE).getString("news", null);
        Type type = new TypeToken<ArrayList<News>>() {
        }.getType();
        savedNews = new Gson().fromJson(newsJSONString, type);
        return savedNews;
    }

    public void savePrefCollection(ArrayList<Collection> collections) {
        preferences = context.getSharedPreferences("collection", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        prefsEditor.remove("collection");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(collections);
        prefsEditor.putString("collection", json);
        prefsEditor.commit();
    }


    public ArrayList<Collection> getPrefCollection() {
        preferences = context.getSharedPreferences("collection", Context.MODE_PRIVATE);
        ArrayList<Collection> savedCollection;
        String catalogsJSONString = context.getSharedPreferences("collection", Context.MODE_PRIVATE).getString("collection", null);
        Type type = new TypeToken<ArrayList<Collection>>() {
        }.getType();
        savedCollection = new Gson().fromJson(catalogsJSONString, type);
        return savedCollection;
    }


}
