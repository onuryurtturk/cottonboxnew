package batunet.cottonboxnav.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;


import java.io.File;
import java.util.ArrayList;

import batunet.cottonboxnav.Catalog;
import batunet.cottonboxnav.models.Pages;

/**
 * Created by onur on 7.5.2015.
 */
public class DownloadOperations {

    public DownloadOperations(Context context) {
        this.context = context;
    }

    Context context;



    public void downloadCatalogs(ArrayList<Catalog> myCatalogs)
    {
        String localUrl;
        if (!(myCatalogs.size() == 0)) {

            for (int i = 0; i < myCatalogs.size(); i++)
            {
                localUrl=downloadFile(myCatalogs.get(i).getCover_original_url(),myCatalogs.get(i).getCover_file_name(),myCatalogs.get(i).getId());
                myCatalogs.get(i).setCover_original_url(localUrl);
            }
        }
    }

    public void downloadPages(ArrayList<Pages> catalogPages)
    {
        String localUrl;
        if (!(catalogPages.size() == 0)) {

            for (int i = 0; i < catalogPages.size(); i++)
            {
                localUrl=downloadFile(catalogPages.get(i).getPhoto_medium_url(),catalogPages.get(i).getPhoto_file_name(),catalogPages.get(i).getCatalog_id());
                catalogPages.get(i).setPhoto_medium_url(localUrl);
                localUrl=downloadFile(catalogPages.get(i).getPhoto_original_url(),"original" + catalogPages.get(i).getPhoto_file_name(),catalogPages.get(i).getCatalog_id());
                catalogPages.get(i).setPhoto_original_url(localUrl);
            }
        }



    }

    public String downloadFile(String uRl,String filename,String catalogId) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/Cotton/"+catalogId);

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Demo")
                .setDescription("Something useful. No, really.")
                .setDestinationInExternalPublicDir("/Cotton/" + catalogId, filename);

        mgr.enqueue(request);

        return Environment.getExternalStorageDirectory() + "/Cotton/" + catalogId +"/"+ filename;
    }
}
