package batunet.cottonboxnav.utils;


/**
 * Created by onuryurtturk on 4/20/2015.
 */
public final class Constants {


    public static final String MISSING_URL = "https://www.cottonbox.com.tr";
    public static final String LANGUAGE_URL = "https://www.cottonbox.com.tr/api/languages";
    public static final String LANGUAGE_CUSTOM = "https://www.cottonbox.com.tr/api/allSets?l=";
    public static final String ALLITEMS_URL = "https://www.cottonbox.com.tr/api/allSets";
    public static final String NEWS_URL = "https://www.cottonbox.com.tr/api/news";
    public static final String MEDIA_URL = "https://www.cottonbox.com.tr/api/media";
    public static final String SLIDER_URL = "https://www.cottonbox.com.tr/api/slider";
    public static final String CATEGORY_URL = "https://www.cottonbox.com.tr/api/collection";
    public static final String HOME_URL = "https://www.cottonbox.com.tr/api/homepageGood?l=";
    public static final String MAIL_URL = "https://www.cottonbox.com.tr/api/mail";
    public static final String CATALOG_URL = "https://www.cottonbox.com.tr/api/catalog";

    private Constants() {
    }

    public static class Config {
        public static final boolean DEVELOPER_MODE = false;
    }

    public static class Extra {
        public static final String FRAGMENT_INDEX = "com.nostra13.example.universalimageloader.FRAGMENT_INDEX";
        public static final String IMAGE_POSITION = "com.nostra13.example.universalimageloader.IMAGE_POSITION";
    }
}