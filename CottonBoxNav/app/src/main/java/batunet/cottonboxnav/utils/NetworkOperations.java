package batunet.cottonboxnav.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by onuryurtturk on 5/14/2015.
 */
public class NetworkOperations {

    public NetworkOperations(Context context) {
        this.context = context;
    }

    public Context context;


    public boolean isNetworkConnected() {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


}
