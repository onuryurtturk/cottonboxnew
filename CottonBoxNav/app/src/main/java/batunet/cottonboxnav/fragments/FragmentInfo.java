package batunet.cottonboxnav.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import batunet.cottonboxnav.R;

//import android.support.v4.app.Fragment;

/**
 * Created by onuryurtturk on 4/18/2015.
 */
public class FragmentInfo extends Fragment {

    MapView mapView;
    private GoogleMap map;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView=inflater.inflate(R.layout.layout_info,container,false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mapView=(MapView)rootView.findViewById(R.id.cotton_map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        //MapsInitializer.initialize(getActivity().getApplicationContext());
        //MapsInitializer.initialize(rootView.getContext());
        try
        {
            MapsInitializer.initialize(getActivity().getApplicationContext());

            if(map==null)
            {
                map = ((MapView)rootView.findViewById(R.id.cotton_map)).getMap();
            }
        }
        catch (Exception e)
        {
            Toast.makeText(getActivity().getApplicationContext(),"Map Yüklenirken Hata oluştu",Toast.LENGTH_LONG);
        }

        //map=mapView.getMap();

        double lat = 37.812357;
        double lon = 29.064460;

        MarkerOptions marker=new MarkerOptions().position(new LatLng(lat,lon)).title("Cotton Box");

        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        map.addMarker(marker);

        CameraPosition position = new CameraPosition.Builder().target(new LatLng(lat,lon)).zoom(12).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(position));



        return  rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
