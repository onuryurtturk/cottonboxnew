package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import batunet.cottonboxnav.NewsDetailActivity;
import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.NewsGridAdapter;
import batunet.cottonboxnav.models.News;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 24.7.2015.
 */
public class FragmentMedia extends Fragment {

    Context context;
    private View view = null;
    FragmentTabHost tabHost;
    ActionBar mybar;
    TextView actionText;
    int tab_index;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_media, container, false);
        context = view.getContext();
        tab_index = getArguments().getInt("index");
        tabHost = (FragmentTabHost) view.findViewById(R.id.mytabhost);
        tabHost.setup(context, getChildFragmentManager(), R.id.realtabcontent);

        tabHost.addTab(tabHost.newTabSpec(getActivity().getResources().getString(R.string.tab_tv)).setIndicator(getActivity().getResources().getString(R.string.tab_tv)), FragmentTv.class, null);
        tabHost.addTab(tabHost.newTabSpec(getActivity().getResources().getString(R.string.tab_press)).setIndicator(getActivity().getResources().getString(R.string.tab_press)), FragmentPress.class, null);

        tabHost.setCurrentTab(tab_index);
        initActionBar();


        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView) view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.title_media_activity));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}