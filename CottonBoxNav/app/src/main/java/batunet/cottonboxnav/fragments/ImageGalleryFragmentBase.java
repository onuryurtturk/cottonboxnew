package batunet.cottonboxnav.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;



import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import batunet.cottonboxnav.ImageActivity;
import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Pages;
import batunet.cottonboxnav.utils.Constants;

/**
 * Created by onuryurtturk on 4/20/2015.
 */
public class ImageGalleryFragmentBase extends FragmentBase {

    public static final int INDEX = 3;

    private Context context;
    private String catalog_id;
    private static String url = "http://dev.mobilkatalog.net/api/v1/catalogs";
    private static String full_url;
    public static ArrayList<Pages> catPages;
    public static ArrayList<String> images;
    private DisplayImageOptions options;
    private ImageLoader imageLoader;
    private ProgressBar spinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        catalog_id = getArguments().getString("catalog_id");

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {

            getActivity().onBackPressed();
            getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));


            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("deprecation")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        catPages = new ArrayList<Pages>();
        catalog_id = getArguments().getString("catalog_id");
        catPages = (ArrayList<Pages>) getArguments().getSerializable("catPages");
        full_url = url + "/" + catalog_id;

        View rootView = inflater.inflate(R.layout.fr_image_gallery, container, false);
        Gallery gallery = (Gallery) rootView.findViewById(R.id.gallery);
        context = rootView.getContext();

        /*gallery.onFling(new GestureDetector(getActivity(),
                new GestureDetector.SimpleOnGestureListener() {

                    @Override
                    public boolean onDown(MotionEvent e) {
                        return true;
                    }

                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

                        return false;
                    }
                    }));*/






        if ((!isNetworkConnected() && catPages == null)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                    .setTitle(getResources().getString(R.string.app_connection_title))
                    .setMessage(getResources().getString(R.string.catalog_connection_warning))
                    .setCancelable(false)
                    .setNeutralButton(getResources().getString(R.string.app_button_okay), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            android.os.Process.killProcess(android.os.Process.myPid());
                        }
                    });
            alertDialog.show();


        } else {

            getImages(catPages);
            setHasOptionsMenu(true);

            Toast.makeText(getActivity().getApplicationContext(),
                    getResources().getString(R.string.catalog_detail_warning), Toast.LENGTH_LONG).show();

            options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.ic_empty)
                    .showImageOnFail(R.drawable.ic_error)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(options)
                    .memoryCacheSize(100 * 1024 * 1024)
                    .diskCacheSize(100 * 1024 * 1024)
                    .threadPoolSize(5)
                    .writeDebugLogs()
                    .build();

            imageLoader = ImageLoader.getInstance();
            imageLoader.init(config);



            gallery.setAdapter(new ImageAdapter());
            gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startImagePagerActivity(position);

                }
            });
        }

        return rootView;
    }


    private void getImages(ArrayList<Pages> catalogPages) {
        images = new ArrayList<String>();
        for (int i = 0; i < catalogPages.size(); i++) {
            images.add(catalogPages.get(i).getPhoto_medium_url());
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void startImagePagerActivity(int position) {
        Intent intent = new Intent(getActivity(), ImageActivity.class);
        intent.putExtra(Constants.Extra.FRAGMENT_INDEX, ImagePagerFragmentBase.INDEX);
        intent.putExtra("catalog_id", catalog_id);
        intent.putExtra(Constants.Extra.IMAGE_POSITION, position);
        startActivity(intent);
    }

    private class ImageAdapter extends BaseAdapter {

        private LayoutInflater inflater;

        ImageAdapter() {
            inflater = LayoutInflater.from(getActivity());
        }

        @Override
        public int getCount() {

            return images.size();

        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View galleryView = convertView;

            if (galleryView == null) {

                galleryView = inflater.inflate(R.layout.item_gallery_image, parent, false);

            }
            ImageView imageView = (ImageView) galleryView.findViewById(R.id.image);
            spinner = (ProgressBar) galleryView.findViewById(R.id.gallery_loading);



            String uri =  (images.get(position)).toString();
            String decodedUri = Uri.decode(uri);



            imageLoader.displayImage("file://"+decodedUri, imageView, options, new SimpleImageLoadingListener() {

                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    spinner.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    String message = null;
                    switch (failReason.getType()) {
                        case IO_ERROR:
                            message = "Lütfen İnternet Bağlantınızı Kontrol edin!";
                            break;
                        case DECODING_ERROR:
                            message = "Decode Edilemedi";
                            break;
                        case NETWORK_DENIED:
                            message = "İndirme Reddedildi";
                            break;
                        case OUT_OF_MEMORY:
                            message = "Bellek Dolu";
                            break;
                        case UNKNOWN:
                            message = "Bilinmeyen Bir Hata Oluştu";
                            break;
                    }
                    Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
                    spinner.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    spinner.setVisibility(View.GONE);
                } }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current, int total) {

                    }
                });



            return galleryView;
        }
    }
}