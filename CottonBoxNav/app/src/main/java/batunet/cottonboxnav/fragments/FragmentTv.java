package batunet.cottonboxnav.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.TvGridAdapter;
import batunet.cottonboxnav.models.Channel;
import batunet.cottonboxnav.models.Series;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onuryurtturk on 5/18/2015.
 */
public class FragmentTv extends Fragment {


    private View view = null;
    Context context;
    private static ArrayList<Channel> channels;
    private TvGridAdapter adapter;
    private GridView tvGrid;
    NetworkOperations operations;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    TextView actionText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {

            view = inflater.inflate(R.layout.fragment_media, container, false);
            context = view.getContext();
            operations = new NetworkOperations(context);
            channels = new ArrayList<>();
            tvGrid = (GridView) view.findViewById(R.id.media_list);

            if (operations.isNetworkConnected()) {
                new jsonMediaTask().execute();
            } else {

                SharedPrefUtils utils = new SharedPrefUtils(context);
                try {
                    channels = utils.getPrefChannel();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter = new TvGridAdapter(context, channels);
                tvGrid.setAdapter(adapter);

            }

            tvGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Channel channelItem = channels.get(position);
                    Fragment frg = new FragmentSeries();
                    Bundle bundle = new Bundle();
                    final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    bundle.putSerializable("series_list", channelItem.getSeriesList());
                    bundle.putString("channel_name", channelItem.getName());
                    frg.setArguments(bundle);
                    ft.replace(R.id.content_frame, frg);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
            initActionBar();


        } catch (InflateException e) {
            Log.e("InflaterProblem", "FragmentTv");

        }

        return view;
    }

    private void cacheAllImages() {
        options = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.ic_menu)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(10)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


        for (int k = 0; k < channels.size(); k++) {
            imageLoader.loadImage((channels.get(k)).getImage_url(), new SimpleImageLoadingListener());

        }
    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView) view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.menu_tv));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private class jsonMediaTask extends AsyncTask<String, String, JSONArray> {

        JsonParser jParser;
        JSONObject mediaItem;
        JSONObject mediaObject;
        JSONArray mediaJsonArray;
        ProgressDialog pDialog;


        @Override
        protected void onPreExecute() {

            pDialog = null;
            pDialog = new ProgressDialog(context);
            pDialog.setMessage(getResources().getString(R.string.dialog_message));
            pDialog.setTitle(getResources().getString(R.string.dialog_title));
            pDialog.setCancelable(false);
            pDialog.show();
        }


        @Override
        protected JSONArray doInBackground(String... params) {

            jParser = new JsonParser();
            mediaObject = jParser.getJSONObjectFromUrl(Constants.MEDIA_URL);

            try {
                mediaJsonArray = mediaObject.getJSONArray("channels");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Channel channelItem;
            JSONArray jsonSeries;
            ArrayList<Series> seriesArrayList = new ArrayList<>();
            ArrayList<String> series_images;

            try {
                for (int i = 0; i < mediaJsonArray.length(); i++) {

                    channelItem = new Channel();
                    mediaItem = (JSONObject) mediaJsonArray.get(i);
                    channelItem.setId(Integer.parseInt(mediaItem.get("id").toString()));
                    channelItem.setName(mediaItem.get("name").toString());
                    channelItem.setImage_url(Constants.MISSING_URL + mediaItem.get("image_url").toString());
                    jsonSeries = mediaItem.getJSONArray("series");

                    Series series;
                    JSONObject seriesObject;
                    seriesArrayList = new ArrayList<>();
                    for (int j = 0; j < jsonSeries.length(); j++) {
                        series = new Series();
                        series_images = new ArrayList<>();
                        seriesObject = (JSONObject) jsonSeries.get(j);
                        series.setId(Integer.parseInt(seriesObject.get("id").toString()));
                        series.setName(seriesObject.get("name").toString());
                        series.setThumb_image_url(seriesObject.get("thumb_image_url").toString());
                        String value = seriesObject.get("original_image_url").toString();
                        series.setJsonImages(value);
                        series.setImage_details(String.valueOf(seriesObject.get("detail")));
                        JSONArray mediaArray = new JSONArray(value);

                        if (mediaArray.length() > 0) {
                            series.setThumb_image_url(Constants.MISSING_URL + String.valueOf(mediaArray.get(0)));
                            series.setOriginal_image_url(Constants.MISSING_URL + String.valueOf(mediaArray.get(0)));

                            for (int m = 0; m < mediaArray.length(); m++) {
                                series_images.add(Constants.MISSING_URL + String.valueOf(mediaArray.get(m)));
                            }
                            series.setImages(series_images);

                        }
                        series.setDescription(seriesObject.get("detail").toString());
                        series.setChannel_id(Integer.parseInt(seriesObject.get("channel_id").toString()));
                        seriesArrayList.add(series);

                    }

                    channelItem.setSeriesList(seriesArrayList);
                    channels.add(channelItem);
                }

            } catch (Exception e) {
                Log.e("ASNYCTASK", "DoInBackgroundError");
            }

            return mediaJsonArray;
        }


        @Override
        protected void onPostExecute(JSONArray jsonArray) {

            pDialog.dismiss();
            adapter = new TvGridAdapter(context, channels);
            tvGrid.setAdapter(adapter);
            cacheAllImages();
            SharedPrefUtils utils = new SharedPrefUtils(context);
            utils.savePrefChannel(channels);

        }

        @Override
        protected void onProgressUpdate(String... values) {
        }


    }


}
