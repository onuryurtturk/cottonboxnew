package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.SeriesImageAdapter;
import batunet.cottonboxnav.models.Series;
import batunet.cottonboxnav.utils.Constants;

/**
 * Created by onur on 26.7.2015.
 */
public class FragmentSeriesDetail extends Fragment {

    Context context;
    private View view = null;
    ActionBar mybar;
    private ViewPager series_slider;
    private String series_descriptions, json_images, serie_name,series_image_details;
    private int series_id;
    private ArrayList<String> seriesImages,seriesDetails;
    private ArrayList<Series> seriesList;
    private SeriesImageAdapter series_slider_adapter;
    private CirclePageIndicator seriesIndicator;
    TextView actionText;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_series_detail, container, false);
        context = view.getContext();

        seriesList = new ArrayList<>();
        //seriesImages = getIntent().getStringArrayListExtra("series_images");
        series_image_details = getArguments().getString("series_image_details");
        json_images = getArguments().getString("series_json");
        seriesImages = getSeriesImages(json_images);
        seriesDetails = getImageDetails(series_image_details);

        serie_name = getArguments().getString("series_name");
        series_descriptions = getArguments().getString("series_text");
        series_id = getArguments().getInt("series_id", 0);

        if (seriesImages == null) {
            Log.e("SERIES", "SERIES ARRAY SIZE" + "NULL");
        } else {
            Log.e("SERIES", "SERIES ARRAY SIZE" + String.valueOf(seriesImages.size()));
            Log.e("SERIES", "SERIES DESC" + series_descriptions);

            series_slider = (ViewPager) view.findViewById(R.id.series_pager);
            series_slider_adapter = new SeriesImageAdapter(context, seriesImages, seriesDetails);
            series_slider.setAdapter(series_slider_adapter);

            seriesIndicator = (CirclePageIndicator) view.findViewById(R.id.series_indicator);
            seriesIndicator.setViewPager(series_slider);
        }

        initActionBar();

        return view;
    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView) view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(serie_name);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    private ArrayList<String> getSeriesImages(String value) {
        ArrayList<String> images = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(value);
            for (int m = 0; m < jsonArray.length(); m++) {
                images.add(Constants.MISSING_URL + String.valueOf(jsonArray.get(m)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return images;
    }

    private ArrayList<String> getImageDetails(String value) {
        ArrayList<String> imageDeatils = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(value);
            for (int m = 0; m < jsonArray.length(); m++) {
                imageDeatils.add(String.valueOf(jsonArray.get(m)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return imageDeatils;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}