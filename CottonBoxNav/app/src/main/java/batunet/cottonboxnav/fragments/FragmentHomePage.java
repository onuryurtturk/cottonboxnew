package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.SetsActivity;
import batunet.cottonboxnav.adapters.HomePageAdapter;
import batunet.cottonboxnav.adapters.SliderAdapter;
import batunet.cottonboxnav.models.Home;
import batunet.cottonboxnav.models.Language;
import batunet.cottonboxnav.models.Product;
import batunet.cottonboxnav.models.Slider;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 24.7.2015.
 */
public class FragmentHomePage  extends Fragment {

    Context context;
    private View view = null;
    FragmentTabHost tabHost;
    ActionBar mybar;

    private GridView homeGrid;
    private HomePageAdapter homeGridAdapter;
    private ViewPager slider;
    private SliderAdapter sliderAdapter;

    public static ArrayList<Home> homeItems;
    int currentPage = 0;
    Timer swipeTimer;
    public ArrayList<Language> languages;
    private ArrayList<Slider> sliderImages;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    NetworkOperations operations;
    CirclePageIndicator catIndicator;
    AlertDialog.Builder builder;
    AlertDialog alert;
    SharedPrefUtils utils;
    Language appLanguage;
    TextView actionText;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_home, container, false);
        context = view.getContext();
        homeGrid = (GridView) view.findViewById(R.id.home_grid);
        slider = (ViewPager) view.findViewById(R.id.home_pager);
        catIndicator = (CirclePageIndicator) view.findViewById(R.id.catalog_indicator);
        operations = new NetworkOperations(context);
        utils = new SharedPrefUtils(context);
        languages = utils.getPrefLanguage();
        appLanguage = languages.get(0);



        homeItems = new ArrayList<>();
        sliderImages = new ArrayList<>();
        /*mybar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        mybar.setDisplayHomeAsUpEnabled(true);
        mybar.setHomeButtonEnabled(true);
        mybar.setTitle(R.string.title_main_activity);*/
        initActionBar();


        if (operations.isNetworkConnected() == true) {
            new homePageTask().execute(Constants.HOME_URL + appLanguage.getLanguage_id());
        } else {

            showAlert(context);
            SharedPrefUtils utils = new SharedPrefUtils(context);
            homeItems = utils.getPrefHome();
            sliderImages = utils.getPrefSlider();
            sliderAdapter = new SliderAdapter(context, sliderImages);
            slider.setAdapter(sliderAdapter);
            //slider.setPageTransformer(true, new RotateUpTransformer());
            homeGridAdapter = new HomePageAdapter(context, homeItems);
            homeGrid.setAdapter(homeGridAdapter);
            catIndicator.setViewPager(slider);
            disMissAlert();

        }

        homeGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Home productItem = homeItems.get(position);
                Intent intent = new Intent(context, SetsActivity.class);
                intent.putExtra("product_id", (String.valueOf(productItem.getProduct_id())));
                intent.putExtra("product_name",(String.valueOf(productItem.getTitle())));
                context.startActivity(intent);
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        final Handler handler = new Handler();

        slider.beginFakeDrag();
        Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == sliderImages.size() - 1) {
                    currentPage = 0;

                } else {
                    currentPage++;
                }
                slider.setCurrentItem(currentPage, false);


            }
        };
        swipeTimer = new Timer();
        final Runnable run = update;
        swipeTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(run);
            }
        }, 400, 4000);


        return view;
    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View view =  getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.title_main_activity));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }





    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void disMissAlert() {

        if (alert != null) {
            alert.dismiss();
        }
    }


    private android.support.v7.app.AlertDialog showAlert(Context context) {
        View dialoglayout = getActivity().getLayoutInflater().inflate(R.layout.custom_alert_dialog, null);
        builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setView(dialoglayout);
        builder.setCancelable(false);
        alert = builder.create();
        alert.show();

        return alert;
    }

    private class homePageTask extends AsyncTask<String, String, JSONArray> {

        JsonParser jParser;
        JSONObject homeItem,sliderItem;
        JSONArray homeJsonArray;

        @Override
        protected void onPreExecute() {
            showAlert(context);
        }


        @Override
        protected JSONArray doInBackground(String... params) {

            jParser = new JsonParser();
            homeJsonArray = jParser.getJSONArrayFromUrl(Constants.HOME_URL + appLanguage.getLanguage_id());
            Home home;

            try {
                for (int i = 0; i < homeJsonArray.length(); i++) {
                    homeItem = (JSONObject) homeJsonArray.get(i);
                    home = new Home();
                    home.setId(homeItem.getInt("id"));
                    home.setLanguage_id(homeItem.getInt("language_id"));
                    home.setTitle(homeItem.getString("title"));
                    home.setSubTitle(homeItem.getString("subtitle"));
                    home.setContent(homeItem.getString("content"));
                    home.setImage(Constants.MISSING_URL + homeItem.getString("image"));
                    homeItem = (JSONObject) ((JSONObject) homeJsonArray.get(i)).get("set");
                    home.setProduct_id(homeItem.getInt("id"));

                    homeItems.add(home);
                }


                homeJsonArray = jParser.getJSONArrayFromUrl(Constants.SLIDER_URL);

                Slider slider;
                    for (int i = 0; i < homeJsonArray.length(); i++) {
                        sliderItem = (JSONObject) homeJsonArray.get(i);
                        slider = new Slider();
                        slider.setUrl(sliderItem.getString("url"));
                        sliderItem = (JSONObject) sliderItem.get("text");
                        slider.setText(sliderItem.getString(appLanguage.getLanguage_short()));
                        sliderImages.add(slider);
                    }

            } catch (Exception e) {
                Log.e("ASNYCTASK", "DoInBackgroundError");
            }

            return homeJsonArray;
        }


        @Override
        protected void onPostExecute(JSONArray jsonArray) {

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            utils.savePrefHome(homeItems);
            utils.savePrefSlider(sliderImages);

            sliderAdapter = new SliderAdapter(context, sliderImages);
            slider.setAdapter(sliderAdapter);
            //slider.setPageTransformer(true, new RotateUpTransformer());
            homeGridAdapter = new HomePageAdapter(context, homeItems);
            homeGrid.setAdapter(homeGridAdapter);
            catIndicator.setViewPager(slider);
            disMissAlert();

            imageLoader = ImageLoader.getInstance();


            if(utils.getFirstRun() == 1 || utils.getOfflineMode() == 1)
            {
                for(int i=0; i < homeItems.size(); i++)
                {
                    imageLoader.loadImage(homeItems.get(i).getImage(), new SimpleImageLoadingListener());

                }

            }


            /*JsonCategoryParse categoryTask = new JsonCategoryParse();
            categoryTask.execute(Constants.CATEGORY_URL);*/
        }

    }
}