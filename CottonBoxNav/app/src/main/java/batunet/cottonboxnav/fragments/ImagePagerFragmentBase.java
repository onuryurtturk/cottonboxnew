package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.lang.reflect.Type;
import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.models.Pages;
import batunet.cottonboxnav.utils.Constants;

/**
 * Created by onuryurtturk on 4/20/2015.
 */
public class ImagePagerFragmentBase extends FragmentBase {

    public static final int INDEX = 2;
    ArrayList<String> pages;
    ArrayList<Pages> catPages;
    DisplayImageOptions options;
    ImageLoader imageLoader;
    SharedPreferences favPref;
    Context context;
    ArrayList<Pages> favorites;
    static int favPosition;


    Fragment fr = null;
    String tag ="";
    int titleRes;



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        getActivity().getMenuInflater().inflate(R.menu.fav_menu, menu);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        catPages = new ArrayList<Pages>();
        pages = new ArrayList<String>();

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fr_image_pager, container, false);
        ViewPager pager = (ViewPager) rootView.findViewById(R.id.pager);

        catPages = (ArrayList<Pages>) (getArguments().getSerializable("catPages"));
        context = rootView.getContext();
        favPref = context.getSharedPreferences("favorite", context.MODE_PRIVATE);
        favorites = getFavPreferences();
        pages = getImages(catPages);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

        try {
            if (favorites == null) {
                favorites = new ArrayList<Pages>();
            }
        } catch (Exception e) {
            Log.e("Favorites", "Favorites List Empty");
        }


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(rootView.getContext())
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(100 * 1024 * 1024)
                .diskCacheSize(100 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);



        pager.setAdapter(new ImageAdapter());
        pager.setCurrentItem(getArguments().getBundle("bundle").getInt(Constants.Extra.IMAGE_POSITION, 0));
        return rootView;
    }

    public ArrayList getImages(ArrayList<Pages> pageList) {
        ArrayList images = new ArrayList();

        for (int i = 0; i < pageList.size(); i++) {
            images.add(pageList.get(i).getPhoto_original_url());
        }

        return images;
    }

    private class ImageAdapter extends PagerAdapter {

        private LayoutInflater inflater;

        ImageAdapter() {
            inflater = LayoutInflater.from(getActivity());
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return pages.size();
        }


        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
            assert imageLayout != null;
            favPosition = position;
            ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
            final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);

            String uri =  (pages.get(position)).toString();
            String decodedUri = Uri.decode(uri);

            imageLoader.displayImage("file://"+decodedUri, imageView, options, new SimpleImageLoadingListener() {

                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    spinner.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    String message = null;
                    switch (failReason.getType()) {
                        case IO_ERROR:
                            message = "Lütfen İnternet Bağlantınızı Kontrol edin!";
                            break;
                        case DECODING_ERROR:
                            message = "Decode Edilemedi";
                            break;
                        case NETWORK_DENIED:
                            message = "İndirme Reddedildi";
                            break;
                        case OUT_OF_MEMORY:
                            message = "Bellek Dolu";
                            break;
                        case UNKNOWN:
                            message = "Bilinmeyen Bir Hata Oluştu";
                            break;
                    }
                    Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
                    spinner.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    spinner.setVisibility(View.GONE);
                }
            });


            view.addView(imageLayout, 0);
            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {

                getActivity().onBackPressed();
                getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                return true;
            }
            case R.id.action_fav: {
                if (catPages != null) {

                    favorites.add(catPages.get(favPosition - 1));
                    saveFavPreferences(findPreferences(favorites));
                    Toast.makeText(context, "Katalog Sayfası Favorilere Eklendi", Toast.LENGTH_LONG).show();

                }
                return true;

            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Pages> findPreferences(ArrayList<Pages> favs) {
        ArrayList<Pages> findPages = getFavPreferences();
        int i = 0, j = 0;
        try {
            if (findPages.size() > 0) {
                for (j = 0; j < favs.size(); j++) {

                    if (!foundInOlds(favs.get(j))) {
                        findPages.add(favs.get(j));
                    }
                }

            } else {
                //findPages=new ArrayList<Pages>();
                //findPages.addAll(favs);
            }

        } catch (Exception e) {
            findPages = new ArrayList<Pages>();
            findPages.addAll(favs);
        }
        return findPages;
    }

    private boolean foundInOlds(Pages page) {

        ArrayList<Pages> findPages = getFavPreferences();
        for (int i = 0; i < findPages.size(); i++) {
            if (page.getId().equals(findPages.get(i).getId())) {
                return true;
            }

        }
        return false;
    }

    private void saveFavPreferences(ArrayList<Pages> favs) {
        SharedPreferences.Editor prefsEditor = favPref.edit();
        prefsEditor.remove("favorites");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(favs);
        prefsEditor.putString("favorites", json);
        prefsEditor.commit();
    }

    private ArrayList<Pages> getFavPreferences() {
        ArrayList<Pages> saveFavs;
        String catalogsJSONString = context.getSharedPreferences("favorite", context.MODE_PRIVATE).getString("favorites", null);
        Type type = new TypeToken<ArrayList<Pages>>() {
        }.getType();
        saveFavs = new Gson().fromJson(catalogsJSONString, type);
        return saveFavs;
    }
}