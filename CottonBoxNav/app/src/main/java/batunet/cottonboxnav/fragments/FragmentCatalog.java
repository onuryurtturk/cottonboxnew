package batunet.cottonboxnav.fragments;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.CatalogPagerAdapter;
import batunet.cottonboxnav.models.CatalogF;
import batunet.cottonboxnav.utils.ActivityResultBus;
import batunet.cottonboxnav.utils.ActivityResultEvent;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 27.7.2015.
 */
public class FragmentCatalog extends Fragment {

    Context context;
    private View view = null;
    ActionBar mybar;
    public ArrayList<CatalogF> catalogList;
    public ArrayList<String> org_images;
    SharedPreferences mPrefs, mFirst;
    private ProgressDialog mDialog;
    SharedPrefUtils utils;
    ViewPager catalogPager;
    CatalogPagerAdapter catalogAdapter;
    NetworkOperations operations;
    int downstatus;
    ContextWrapper contextWrapper;
    BroadcastReceiver onComplete;
    File direct;
    jsonCatalogParse catalogTask;
    TextView actionText;
    Handler handler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_catalog, container, false);
        context = view.getContext();
        catalogPager = (ViewPager) view.findViewById(R.id.catalog_pager);
        org_images = new ArrayList<>();
        utils = new SharedPrefUtils(context);
        operations = new NetworkOperations(context);
        contextWrapper = new ContextWrapper(context);
        mFirst = getActivity().getSharedPreferences("first", 0);
        mPrefs = getActivity().getSharedPreferences("catalog", context.MODE_PRIVATE);
        initActionBar();

        catalogList = utils.getPrefCatalog();

        checkCatState();


        return view;
    }

    private void checkCatState() {
        if (operations.isNetworkConnected()) {
            if (catalogList != null) {

                if (catalogList.size() > 0) {
                    catalogAdapter = new CatalogPagerAdapter(context, catalogList);
                    catalogPager.setAdapter(catalogAdapter);
                } else {
                    getCatalogs();
                }
            } else {
                getCatalogs();
            }
        } else {

            if (catalogList != null) {


                if (catalogList.size() > 0) {
                    catalogAdapter = new CatalogPagerAdapter(context, catalogList);
                    catalogPager.setAdapter(catalogAdapter);
                } else {
                    Toast.makeText(context, getResources().getString(R.string.language_warning_net), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(context, getResources().getString(R.string.language_warning_net), Toast.LENGTH_LONG).show();

            }
        }
    }





    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View view =  getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.title_catalog_activity));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }


    @Override
    public void onStart() {
        super.onStart();
        ActivityResultBus.getInstance().register(mActivityResultSubscriber);
    }

    @Override
    public void onStop() {
        super.onStop();
        ActivityResultBus.getInstance().unregister(mActivityResultSubscriber);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            downstatus = data.getIntExtra("downloaded", 0);
            catalogList = utils.getPrefCatalog();
            catalogAdapter = new CatalogPagerAdapter(context, catalogList);
            catalogPager.setAdapter(catalogAdapter);
        }
    }

    private Object mActivityResultSubscriber = new Object() {
        @Subscribe
        public void onActivityResultReceived(ActivityResultEvent event) {
            int requestCode = event.getRequestCode();
            int resultCode = event.getResultCode();
            Intent data = event.getData();
            onActivityResult(requestCode, resultCode, data);
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void downloadCatalogs(ArrayList<CatalogF> myCatalogs) {
        String localUrl;
        if (!(myCatalogs.size() == 0)) {
            CatalogF catalogF = new CatalogF();
            ArrayList<String> cPages;
            for (int i = 0; i < myCatalogs.size(); i++) {

                catalogF = myCatalogs.get(i);
                cPages = catalogF.getPages();
                localUrl = downloadFile(cPages.get(0), catalogF.getName());
                myCatalogs.get(i).setCover_page(localUrl);
            }
        }
    }


    public void getCatalogs() {

        /*mDialog = new ProgressDialog(context);
        mDialog.setMessage("Yükleniyor Lütfen Bekleyiniz...");
        mDialog.setTitle("Kataloglar Getiriliyor...");
        mDialog.setCancelable(false);
        mDialog.show();*/

        catalogTask = new jsonCatalogParse();
        catalogTask.execute(Constants.CATALOG_URL);
    }


    public String downloadFile(String uRl, String filename) {

        direct = new File(Environment.getExternalStorageDirectory() + "/Cotton/");
        //File direct = new File(contextWrapper.getFilesDir().getPath() + "/cotton/");


        if (!direct.exists()) {
            direct.mkdirs();

        } else {
            deleteDir(direct);
        }

        DownloadManager mgr = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);

        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(true).setTitle("CottonBox Android App")
                .setDescription("Katalog İndiriliyor...")
                        //.setDestinationInExternalFilesDir(context,"", filename + ".jpg")
                .setDestinationInExternalPublicDir("/Cotton/", filename + ".jpg")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);

        mgr.enqueue(request);

        //return  direct.getPath() +"/" + filename + ".jpg";
        return Environment.getExternalStorageDirectory() + "/Cotton/" + filename + ".jpg";
    }

    public void deleteDir(File path) {
        if (path.isDirectory()) {
            String[] children = path.list();
            for (int i = 0; i < children.length; i++) {
                new File(path, children[i]).delete();
            }
        }
    }


    private class jsonCatalogParse extends AsyncTask<String, String, JSONObject> {

        JsonParser jParser;
        JSONObject catalog = null;
        JSONArray catalogPages;
        ArrayList<String> cPages;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            catalogList = new ArrayList<>();
            getActivity().registerReceiver(onComplete,
                    new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        }

        @Override
        protected JSONObject doInBackground(String... args) {

            String json_url = args[0];
            jParser = new JsonParser();
            cPages = new ArrayList<>();
            catalogPages = jParser.getJSONArrayFromUrl(json_url);
            try {
                CatalogF catalog = new CatalogF();
                for (int i = 0; i < catalogPages.length(); i++) {
                    cPages.add(Constants.MISSING_URL + String.valueOf(catalogPages.get(i)));
                    org_images.add(Constants.MISSING_URL + String.valueOf(catalogPages.get(i)));
                }
                catalog.setName(String.valueOf(System.currentTimeMillis()) + String.valueOf(catalogPages.length()));
                catalog.setPage_count(catalogPages.length());
                catalog.setCover_page(cPages.get(0));
                catalog.setOriginal_pages(org_images);
                catalog.setPages(cPages);
                catalogList.add(catalog);
                downloadCatalogs(catalogList);

                //savePreferences(catalogList);

                //downloadPages(catPages);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return catalog;
        }


        @Override
        protected void onPostExecute(JSONObject json) {


           /* utils.savePrefCatalog(catalogList);
            catalogAdapter = new CatalogPagerAdapter(context, catalogList);
            catalogPager.setAdapter(catalogAdapter);*/


            handler = new Handler();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    Log.e("DOWN", "RUN ICINDE");
                    utils.savePrefCatalog(catalogList);
                    catalogAdapter = new CatalogPagerAdapter(context, catalogList);
                    catalogPager.setAdapter(catalogAdapter);
                }
            }, 4000);

            //mDialog.dismiss();
            /*if (direct.isDirectory()) {
                while (!(direct.listFiles().length > 0)) {
                    Log.e("DOWN","WHILE ICINDE");
                    if (direct.listFiles().length > 0) {
                        handler = new Handler();
                        Log.e("DOWN","IF ICINDE");

                        final Runnable r = new Runnable() {
                            public void run() {

                                Log.e("DOWN","RUN ICINDE");
                                utils.savePrefCatalog(catalogList);
                                catalogAdapter = new CatalogPagerAdapter(context, catalogList);
                                catalogPager.setAdapter(catalogAdapter);
                            }
                        };

                        handler.postDelayed(r, 4000);
                        break;


                    }
                }
            } else {
                Toast.makeText(context, "BU bir path değil", Toast.LENGTH_LONG).show();
            }*/



            onComplete = new BroadcastReceiver() {
                public void onReceive(Context ctxt, Intent intent) {
                    Log.e("Download Completed", "DOWNLOAD");
                    utils.savePrefCatalog(catalogList);
                    catalogAdapter = new CatalogPagerAdapter(context, catalogList);
                    catalogPager.setAdapter(catalogAdapter);

                }
            };

        }


    }

}