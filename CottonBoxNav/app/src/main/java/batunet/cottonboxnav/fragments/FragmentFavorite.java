package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.FavoriteGridAdapter;
import batunet.cottonboxnav.models.Pages;

/**
 * Created by onuryurtturk on 4/22/2015.
 */
public class FragmentFavorite extends Fragment {

    private GridView gridView;
    Context context;
    SharedPreferences favPref;
    ArrayList<Pages> favorites;
    ActionBar mybar;
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fav_menu, menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView=inflater.inflate(R.layout.layout_favorite,container,false);
        context = rootView.getContext();
        favPref = context.getSharedPreferences("favorite", context.MODE_PRIVATE);

        mybar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        mybar.setDisplayHomeAsUpEnabled(true);
        mybar.setHomeButtonEnabled(true);
        mybar.setTitle(R.string.title_main_activity);


        gridView=(GridView)rootView.findViewById(R.id.grid_favorites);
        if (getActivity().getApplicationContext() != null) {
            Toast.makeText(getActivity().getApplicationContext(), "hata", Toast.LENGTH_LONG);
        }
        try {
            favorites = getPreferences();
        } catch (Exception e) {
            Log.e("FragmentFav", "favorites null");
        }
        gridView.setAdapter(new FavoriteGridAdapter(rootView.getContext(), favorites));
        return rootView;
    }

    private ArrayList<Pages> getPreferences()
    {
        ArrayList<Pages> saveFavs;
        String catalogsJSONString = context.getSharedPreferences("favorite", context.MODE_PRIVATE).getString("favorites", null);
        Type type = new TypeToken< ArrayList < Pages >>() {}.getType();
        saveFavs = new Gson().fromJson(catalogsJSONString, type);
        return  saveFavs;
    }
}
