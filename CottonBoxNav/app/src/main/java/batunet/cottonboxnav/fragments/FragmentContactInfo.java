package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.utils.NetworkOperations;

/**
 * Created by onur on 26.7.2015.
 */
public class FragmentContactInfo  extends Fragment implements GoogleMap.OnMarkerClickListener {

    Context context;
    private View view = null;
    ActionBar mybar;
    GoogleMap map;
    final Double lat = 37.812094;
    final Double lng = 29.064439;
    NetworkOperations operations;
    TextView actionText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_info, container, false);
        context = view.getContext();
        operations = new NetworkOperations(context);

        initActionBar();

        if (operations.isNetworkConnected() == false) {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle(getResources().getString(R.string.map_warning));
            alert.setMessage(getResources().getString(R.string.map_error_net));
            alert.setNeutralButton(getResources().getString(R.string.map_ok), null);
            alert.show();
        }
        createMap();

        return view;
    }

    private void createMap() {

        try {
            if (map == null) {
                SupportMapFragment mSupportMapFragment = ((SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.fr_map_fragment));

                if (mSupportMapFragment == null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    mSupportMapFragment = SupportMapFragment.newInstance();
                    fragmentTransaction.replace(R.id.fr_map_fragment, mSupportMapFragment).commit();
                }
                else if (mSupportMapFragment != null)
                {
                    map = mSupportMapFragment.getMap();
                    if (map != null)
                    {

                        MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(lat, lng)).title("CottonBox").snippet("Ana Merkez").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        map.setMyLocationEnabled(true);
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 15));
                        map.addMarker(markerOptions);

                    }
                    else if (map == null) {
                        Toast.makeText(context, getResources().getString(R.string.map_error), Toast.LENGTH_LONG);
                    }
                }
            }
        } catch (NullPointerException e) {
            Log.e("MapError", "Map null");
        }
    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View view =  getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.contact_address));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}