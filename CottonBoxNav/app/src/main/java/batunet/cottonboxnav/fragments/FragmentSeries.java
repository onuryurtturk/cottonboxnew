package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.SeriesGridAdapter;
import batunet.cottonboxnav.models.Series;

/**
 * Created by onur on 26.7.2015.
 */
public class FragmentSeries extends Fragment {

    Context context;
    private View view = null;
    private GridView seriesGrid;
    private SeriesGridAdapter seriesGridAdapter;
    public static ArrayList<Series> series;
    ActionBar mybar;
    TextView actionText;
    Series seriesItem;
    String channel_name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_series, container, false);
        context = view.getContext();


        seriesGrid = (GridView) view.findViewById(R.id.series_grid);
        series = new ArrayList<Series>();
        //series = (ArrayList<Series>) getActivity().getIntent().getSerializableExtra("series_list");
        series=(ArrayList<Series>)getArguments().getSerializable("series_list");
        channel_name = getArguments().getString("channel_name");
        seriesGridAdapter = new SeriesGridAdapter(context, series);
        seriesGrid.setAdapter(seriesGridAdapter);
        initActionBar();

        seriesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                seriesItem = series.get(position);
                final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment frg = new FragmentSeriesDetail();
                Bundle bundle = new Bundle();
                bundle.putInt("series_id",seriesItem.getId());
                bundle.putString("series_image_details",seriesItem.getImage_details());
                bundle.putString("series_json", seriesItem.getJsonImages());
                bundle.putStringArrayList("series_images", seriesItem.getImages());
                bundle.putString("series_text", seriesItem.getDescription());
                bundle.putString("series_name", seriesItem.getName());
                frg.setArguments(bundle);
                ft.replace(R.id.content_frame, frg);
                ft.addToBackStack(null);
                ft.commit();

            }
        });



        return view;
    }


    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View view =  getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(channel_name);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}