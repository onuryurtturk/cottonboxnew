package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import batunet.cottonboxnav.ProductsActivity;
import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.CategoryGridAdapter;
import batunet.cottonboxnav.adapters.SliderAdapter;
import batunet.cottonboxnav.models.Channel;
import batunet.cottonboxnav.models.Collection;
import batunet.cottonboxnav.models.Language;
import batunet.cottonboxnav.models.News;
import batunet.cottonboxnav.models.Product;
import batunet.cottonboxnav.models.Sets;
import batunet.cottonboxnav.models.Slider;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 24.7.2015.
 */
public class FragmentCollection extends Fragment {

    Context context;
    private View view = null;
    private GridView categoriesGrid;
    private CategoryGridAdapter categoryGridAdapter;

    public static ArrayList<Collection> collections;
    SharedPreferences prefSlider, prefCollection;
    public static ArrayList<Collection> allCollection;
    public static ArrayList<Product> allProduct;
    public static ArrayList<Sets> allSets;
    public static ArrayList<News> allNews, allPress;
    public static ArrayList<Channel> allChannel;
    public ArrayList<Language> languages;
    private ArrayList<Slider> sliderImages;
    NetworkOperations operations;
    AlertDialog.Builder builder;
    AlertDialog alert;
    SharedPrefUtils utils;
    Language appLanguage;
    TextView actionText;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_main, container, false);
        context = view.getContext();
        //slider = (ViewPager) view.findViewById(R.id.slider_pager);
        categoriesGrid = (GridView) view.findViewById(R.id.category_grid);
        operations = new NetworkOperations(context);

        prefSlider = getActivity().getSharedPreferences("slider", Context.MODE_PRIVATE);
        prefCollection = getActivity().getSharedPreferences("collection", Context.MODE_PRIVATE);

        initArrayList(context);
        initActionBar();

        showAlert(context);
        SharedPrefUtils utils = new SharedPrefUtils(context);
        collections = utils.getPrefCollection();
        /*sliderImages = utils.getPrefSlider();
        sliderAdapter = new SliderAdapter(context, sliderImages);
        slider.setAdapter(sliderAdapter);*/
        categoryGridAdapter = new CategoryGridAdapter(context, collections);
        categoriesGrid.setAdapter(categoryGridAdapter);

        //catIndicator = (CirclePageIndicator) view.findViewById(R.id.catalog_indicator);
        //catIndicator.setViewPager(slider);

        disMissAlert();


        categoriesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Collection item = collections.get(position);
                Intent intent = new Intent(context, ProductsActivity.class);
                intent.putExtra("collection_id", item.getId());
                intent.putExtra("collection_name", item.getTitle());
                context.startActivity(intent);


            }
        });

        return view;
    }



    private AlertDialog showAlert(Context context) {
        View dialoglayout = getActivity().getLayoutInflater().inflate(R.layout.custom_alert_dialog, null);
        builder = new AlertDialog.Builder(context);
        builder.setView(dialoglayout);
        builder.setCancelable(false);
        alert = builder.create();
        alert.show();

        return alert;
    }

    private void disMissAlert() {

        if (alert != null) {
            alert.dismiss();
        }
    }

    private void initArrayList(Context context) {
        utils = new SharedPrefUtils(context);
        sliderImages = new ArrayList<>();

        //initialise arraylists
        languages = new ArrayList<>();
        sliderImages = new ArrayList();
        collections = new ArrayList<>();
        allCollection = new ArrayList<>();


        try {
            languages = utils.getPrefLanguage();
            allProduct = utils.getPrefProduct();
            allSets = utils.getPrefSet();
            allNews = utils.getPrefNews();
            allChannel = utils.getPrefChannel();
            allPress = utils.getPrefPress();
            collections = utils.getPrefCollection();
            appLanguage = languages.get(0);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("ARRAYLIST", "error");
        }

    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View view =  getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.title_products_activity));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}