package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 24.7.2015.
 */
public class FragmentSettings extends Fragment {

    Context context;
    private View view = null;
    ActionBar mybar;
    TextView language, offline;
    Switch offline_switch;
    LinearLayout linearLanguage;
    SharedPrefUtils utils;
    AlertDialog.Builder alert;
    TextView actionText;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_settings, container, false);

        context = view.getContext();
        language = (TextView) view.findViewById(R.id.text_language);
        offline = (TextView) view.findViewById(R.id.text_offline);
        offline_switch = (Switch) view.findViewById(R.id.switch_offline);
        linearLanguage = (LinearLayout) view.findViewById(R.id.linear_change);
        alert = new AlertDialog.Builder(context);

        utils = new SharedPrefUtils(context);
        initActionBar();
        language.setText(getResources().getString(R.string.setting_language));
        offline.setText(getResources().getString(R.string.setting_offline));

        offline_switch.setTextOn(getResources().getString(R.string.setting_switch_on));
        offline_switch.setTextOff(getResources().getString(R.string.setting_switch_off));


        if (utils.getOfflineMode() == 1) {
            offline_switch.setChecked(true);
        }


        linearLanguage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                alert.setTitle(getResources().getString(R.string.setting_language_title));
                alert.setMessage(getResources().getString(R.string.setting_language_warning));
                alert.setCancelable(false);
                alert.setPositiveButton(getResources().getString(R.string.map_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        //utils.saveFirstRun(0);
                        utils.saveLanguageState(0);
                        /*Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);*/

                        /*android.os.Process.killProcess(android.os.Process.myPid());
                        finish();*/


                        //---------BURASI FRAGMENT'A GORE DEGİSECEK
                        Intent i = getActivity().getPackageManager().getLaunchIntentForPackage(getActivity().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        //finish();

                    }
                });
                alert.setNegativeButton(getResources().getString(R.string.cache_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
                alert.show();


            }
        });

        offline_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (isChecked) {

                    alert.setTitle(getResources().getString(R.string.cache_title));
                    alert.setMessage(getResources().getString(R.string.cache_message));
                    alert.setCancelable(false);
                    alert.setPositiveButton(getResources().getString(R.string.cache_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            utils.saveOfflineMode(1);

                        }
                    });
                    alert.setNegativeButton(getResources().getString(R.string.cache_cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            offline_switch.setChecked(false);
                            utils.saveOfflineMode(0);
                        }
                    });
                    alert.show();


                } else {
                    utils.saveOfflineMode(0);
                }
            }
        });


        return view;
    }


    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView) view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.title_settings_activity));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}