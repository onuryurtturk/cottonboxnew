package batunet.cottonboxnav.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;

/**
 * Created by onur on 26.7.2015.
 */
public class FragmentContactUs  extends Fragment {

    Context context;
    private View view = null;
    ActionBar mybar;
    TextView actionText;
    Button send;
    EditText name, phone, email, message;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_contact_us, container, false);
        context = view.getContext();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        initActionBar();

        send = (Button) view.findViewById(R.id.btn_send);
        name = (EditText) view.findViewById(R.id.edt_name);
        phone = (EditText) view.findViewById(R.id.edt_telephone);
        email = (EditText) view.findViewById(R.id.edt_email);
        message = (EditText) view.findViewById(R.id.edt_message);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (phone.getText().toString().trim().equals("") && email.getText().toString().trim().equals("") && message.getText().toString().trim().equals(""))
                {

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(getResources().getString(R.string.map_warning));
                    builder.setMessage(getResources().getString(R.string.mail_warning));
                    builder.setNeutralButton(getResources().getString(R.string.map_ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                } else {
                    new sendMessage().execute();
                }
            }
        });

        return view;
    }



    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View view =  getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.contact_email));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class sendMessage extends AsyncTask<String, String, JSONArray> {

        JSONArray JsonArray;
        ProgressDialog pDialog;
        String result = "";
        String p_name,p_phone,p_email,p_message;

        @Override
        protected void onPreExecute() {

            pDialog = null;
            pDialog = new ProgressDialog(context);
            pDialog.setMessage(getResources().getString(R.string.mail_sending));
            pDialog.setTitle(getResources().getString(R.string.mail_wait));
            pDialog.setCancelable(false);
            pDialog.show();

            p_name = String.valueOf(name.getText());
            p_phone = String.valueOf(phone.getText());
            p_email = String.valueOf(email.getText());
            p_message = String.valueOf(message.getText());

        }


        @Override
        protected JSONArray doInBackground(String... params) {

            try {

                JsonParser parser = new JsonParser();
                result = parser.makePost(Constants.MAIL_URL, p_name, p_phone, p_email, p_message);

            } catch (Exception e) {
                Log.e("ASNYCTASK", "DoInBackgroundError");
            }

            return JsonArray;
        }


        @Override
        protected void onPostExecute(JSONArray jsonArray) {

            pDialog.dismiss();
            Log.e("STATUS RESULT", result.toString());

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setNeutralButton(getResources().getString(R.string.map_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();

                }
            });

            if (result.toString().trim().contains("200")) {

                builder.setTitle(getResources().getString(R.string.mail_ok_title));
                builder.setMessage(getResources().getString(R.string.mail_ok));
                builder.show();

            }
            else
            {
                builder.setTitle(getResources().getString(R.string.mail_error_title));
                builder.setMessage(getResources().getString(R.string.mail_error));
                builder.show();


            }
        }
    }
}