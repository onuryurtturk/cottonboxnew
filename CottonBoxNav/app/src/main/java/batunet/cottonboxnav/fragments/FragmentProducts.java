package batunet.cottonboxnav.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.ProductsGridAdapter;
import batunet.cottonboxnav.models.Product;
import batunet.cottonboxnav.models.Sets;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 24.7.2015.
 */
public class FragmentProducts extends Fragment {

    private View view = null;
    Context context;
    private GridView productsGrid;
    private ProductsGridAdapter productsGridAdapter;
    public static String productsUrl = "";
    public static ArrayList<Product> products;
    private String collection_Id;
    ProgressDialog pDialog;
    NetworkOperations operations;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    boolean pauseOnScroll = false;
    boolean pauseOnFling = true;
    ActionBar mybar;
    TextView actionText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_products, container, false);
        context = view.getContext();
        operations = new NetworkOperations(context);


       /* mybar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        mybar.setDisplayHomeAsUpEnabled(true);
        mybar.setHomeButtonEnabled(true);
        mybar.setTitle(R.string.title_news_activity);*/

        // collection_Id = getIntent().getStringExtra("collection_id");
        collection_Id = getArguments().getString("collection_id");
        productsUrl = Constants.CATEGORY_URL + "/" + collection_Id;

        products = new ArrayList<Product>();
        productsGrid = (GridView)view.findViewById(R.id.products_grid);


        if (operations.isNetworkConnected()) {
            new JsonProductsParse().execute(productsUrl);
        } else {
            products = getOfflineData(collection_Id);
            productsGridAdapter = new ProductsGridAdapter(context, products);
            productsGrid.setAdapter(productsGridAdapter);
        }


        return view;
    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View view =  getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.title_products_activity));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }


    private void cacheAllImages() {
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(10)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


        PauseOnScrollListener listener = new PauseOnScrollListener(imageLoader, pauseOnScroll, pauseOnFling);
        productsGrid.setOnScrollListener(listener);

        for (int j = 0; j < products.size(); j++) {
            imageLoader.loadImage((products.get(j)).getOriginal_image_url(), new SimpleImageLoadingListener());

        }

        SharedPrefUtils utils = new SharedPrefUtils(context);
        ArrayList<Sets> mysets = utils.getPrefSet();

        for (int k = 0; k < mysets.size() / 2; k++) {
            imageLoader.loadImage((mysets.get(k)).getImage(), new SimpleImageLoadingListener());

        }
    }


    private ArrayList<Product> getOfflineData(String collection_Id) {
        ArrayList<Product> allProducts = new ArrayList<Product>();
        ArrayList<Product> offlineProducts = new ArrayList<Product>();
        SharedPrefUtils utils = new SharedPrefUtils(context);
        allProducts = utils.getPrefProduct();

        for (int i = 0; i < allProducts.size(); i++) {
            if ((allProducts.get(i)).getCollection_id().equals(collection_Id)) {
                offlineProducts.add(allProducts.get(i));
            }

        }
        return offlineProducts;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class JsonProductsParse extends AsyncTask<String, String, JSONArray> {

        JsonParser jParser;
        JSONObject productsItem;
        JSONArray productsJsonArray;


        @Override
        protected void onPreExecute() {

            pDialog = null;
            pDialog = new ProgressDialog(context);
            pDialog.setMessage(getResources().getString(R.string.dialog_message));
            pDialog.setTitle(getResources().getString(R.string.dialog_title));
            pDialog.setCancelable(false);
            pDialog.show();
        }


        @Override
        protected JSONArray doInBackground(String... params) {

            jParser = new JsonParser();
            productsJsonArray = jParser.getJSONArrayFromUrl(productsUrl);
            Product product;

            try {
                for (int i = 0; i < productsJsonArray.length(); i++) {
                    product = new Product();
                    productsItem = (JSONObject) productsJsonArray.get(i);
                    product.setId(productsItem.getString("id"));
                    product.setTitle(productsItem.getString("title"));
                    product.setDetail(productsItem.getString("detail"));
                    product.setThumb_image_url(productsItem.getString("thumb_image_url"));
                    product.setOriginal_image_url(productsItem.getString("original_image_url"));
                    product.setCollection_id(productsItem.getString("collection_id"));
                    products.add(product);
                }

            } catch (Exception e) {
                Log.e("ASNYCTASK", "DoInBackgroundError");
            }

            return productsJsonArray;
        }


        @Override
        protected void onPostExecute(JSONArray jsonArray) {

            pDialog.dismiss();
            productsGridAdapter = new ProductsGridAdapter(context, products);
            productsGrid.setAdapter(productsGridAdapter);
            //cacheAllImages();
            //new cacheTask().execute();

        }
    }


    private class cacheTask extends AsyncTask<String, String, JSONObject> {


        @Override
        protected JSONObject doInBackground(String... params) {


            try {

                cacheAllImages();
            } catch (Exception e) {
                Log.e("CategoryAsyncTask", "CategoryError");
            }

            return null;
        }

    }
}