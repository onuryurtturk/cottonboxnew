package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.ContactListAdapter;
import batunet.cottonboxnav.models.Contact;
import batunet.cottonboxnav.utils.NetworkOperations;

/**
 * Created by onur on 24.7.2015.
 */
public class FragmentContact  extends Fragment {

    Context context;
    private View view = null;
    private ListView list;
    private ContactListAdapter listAdapter;
    ActionBar mybar;
    TextView actionText;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_contact, container, false);
        context = view.getContext();

        list = (ListView) view.findViewById(R.id.contact_list);

        listAdapter = new ContactListAdapter(context, getContact());
        list.setAdapter(listAdapter);


       /* mybar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        mybar.setDisplayHomeAsUpEnabled(true);
        mybar.setHomeButtonEnabled(true);
        mybar.setTitle(R.string.title_contact_activity);*/

        initActionBar();


    list.setOnItemClickListener(new AdapterView.OnItemClickListener()

    {
        @Override
        public void onItemClick (AdapterView < ? > parent, View view,int position, long id){


        if (position != 3 && position != 0) {

            NetworkOperations operations = new NetworkOperations(context);
            TextView findItem = (TextView) view.findViewById(R.id.contact_text);
            String item = String.valueOf(findItem.getText());
            Fragment frg = new FragmentWeb();
            Bundle bundle = new Bundle();
            final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

            if (item.equals(getResources().getString(R.string.contact_address))) {

                frg = new FragmentContactInfo();
                ft.replace(R.id.content_frame, frg);
                ft.addToBackStack(null);
                ft.commit();

            } else if (item.equals("Facebook")) {


                bundle.putString("url", "https://facebook.com/cottonboxcomtr");
                bundle.putString("name", "Facebook");
                frg.setArguments(bundle);
                ft.replace(R.id.content_frame, frg);
                ft.addToBackStack(null);
                ft.commit();

                if (operations.isNetworkConnected() == false) {
                    Toast.makeText(context, getResources().getString(R.string.language_warning_net), Toast.LENGTH_LONG).show();
                }


            } else if (item.equals(getResources().getString(R.string.contact_email))) {

                frg = new FragmentContactUs();
                ft.replace(R.id.content_frame, frg);
                ft.addToBackStack(null);
                ft.commit();

                if (operations.isNetworkConnected() == false) {
                    Toast.makeText(context, getResources().getString(R.string.language_warning_net), Toast.LENGTH_LONG).show();
                }


            } else if (item.equals("Twitter")) {

                bundle.putString("url", "https://twitter.com/cottonbox");
                bundle.putString("name", "Twitter");
                frg.setArguments(bundle);
                ft.replace(R.id.content_frame, frg);
                ft.addToBackStack(null);
                ft.commit();
                if (operations.isNetworkConnected() == false) {
                    Toast.makeText(context, getResources().getString(R.string.language_warning_net), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    });

        return view;
}

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View view =  getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.title_contact_activity));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }



    private ArrayList<Contact> getContact() {
        ArrayList<Contact> contactsList = new ArrayList<>();
        contactsList.add(new Contact(getResources().getString(R.string.contact_title)));
        contactsList.add(new Contact(R.drawable.ic_map, getResources().getString(R.string.contact_address)));
        contactsList.add(new Contact(R.drawable.ic_cont, getResources().getString(R.string.contact_email)));
        contactsList.add(new Contact(getResources().getString(R.string.contact_follow)));
        contactsList.add(new Contact(R.drawable.ic_facebook, "Facebook"));
        contactsList.add(new Contact(R.drawable.ic_twitter, "Twitter"));

        return contactsList;
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}