package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.NewsGridAdapter;
import batunet.cottonboxnav.models.News;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 24.7.2015.
 */
public class FragmentNews extends Fragment {

    Context context;
    private GridView newsGrid;
    private NewsGridAdapter newsGridAdapter;
    public static final String newsUrl = "https://www.cottonbox.com.tr/api/news";
    private final String MISSING_URL = "https://www.cottonbox.com.tr";
    private View view = null;

    public static ArrayList<News> news;
    NetworkOperations operations;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    SharedPrefUtils utils;
    TextView actionText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }

        view = inflater.inflate(R.layout.activity_news, container, false);
        context = view.getContext();

        operations = new NetworkOperations(context);
        utils = new SharedPrefUtils(context);
        news = new ArrayList<News>();
        newsGrid = (GridView) view.findViewById(R.id.news_grid);
        initActionBar();

        if (operations.isNetworkConnected() && utils.getOfflineMode() == 0) {
            new JsonNewsParse().execute(newsUrl);
        } else {

            SharedPrefUtils utils = new SharedPrefUtils(context);
            news = utils.getPrefNews();
            newsGridAdapter = new NewsGridAdapter(context, news);
            newsGrid.setAdapter(newsGridAdapter);
        }


        return view;
    }

    private void cacheAllImages() {
        options = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.ic_menu)
                .cacheInMemory(false)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .diskCacheSize(150 * 1024 * 1024)
                .threadPoolSize(1)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


        SharedPrefUtils utils = new SharedPrefUtils(context);
        ArrayList<News> mynews = utils.getPrefNews();

        for (int k = 0; k < mynews.size(); k++) {
            imageLoader.loadImage((mynews.get(k)).getOriginal_image_url(), new SimpleImageLoadingListener());

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            getActivity().finish();
            //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View view =  getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.title_news_activity));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }


    private class JsonNewsParse extends AsyncTask<String, String, JSONObject> {

        JsonParser jParser;
        JSONObject newsJsonItem;
        JSONArray newsArray;


        @Override
        protected JSONObject doInBackground(String... params) {


            jParser = new JsonParser();
            newsArray = jParser.getJSONArrayFromUrl(newsUrl);
            News newsItem;

            try {
                for (int i = 0; i < newsArray.length(); i++) {
                    newsItem = new News();
                    newsJsonItem = (JSONObject) newsArray.get(i);
                    newsItem.setId(Integer.parseInt(newsJsonItem.get("id").toString()));
                    newsItem.setTitle(newsJsonItem.get("title").toString());
                    newsItem.setDetail(newsJsonItem.get("detail").toString());
                    if (Integer.parseInt(newsJsonItem.get("is_news").toString()) == 0) {
                        newsItem.setIs_news(false);
                    } else if (Integer.parseInt(newsJsonItem.get("is_news").toString()) == 1) {
                        newsItem.setIs_news(true);
                    }

                    String value = newsJsonItem.get("original_image_url").toString();

                    JSONArray array = new JSONArray(value);

                    if (array.length() > 0) {
                        newsItem.setThumb_image_url(MISSING_URL + String.valueOf(array.get(0)));
                        newsItem.setOriginal_image_url(MISSING_URL + String.valueOf(array.get(0)));
                    }

                    newsItem.setCreated_at(newsJsonItem.get("created_at").toString());
                    news.add(newsItem);
                }

            } catch (Exception e) {
                Log.e("CategoryAsyncTask", "CategoryError");
            }

            return null;
        }


        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            SharedPrefUtils utils = new SharedPrefUtils(context);
            utils.savePrefNews(news);
            newsGridAdapter = new NewsGridAdapter(context, news);
            newsGrid.setAdapter(newsGridAdapter);
            cacheAllImages();

        }


    }
}
