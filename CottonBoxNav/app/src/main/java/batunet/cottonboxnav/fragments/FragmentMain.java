package batunet.cottonboxnav.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.viewpagerindicator.CirclePageIndicator;

import java.lang.reflect.Type;
import java.util.ArrayList;

import batunet.cottonboxnav.Catalog;
import batunet.cottonboxnav.CatalogPagerAdapter;
import batunet.cottonboxnav.R;

/**
 * Created by onuryurtturk on 4/18/2015.
 */
public class FragmentMain extends android.support.v4.app.Fragment {

    ViewPager catalogPager;
    CatalogPagerAdapter catalogAdapter;
    Context context;
    ArrayList<Catalog> catalogs;
    View rootView;
    DisplayImageOptions options;
    ImageLoader imageLoader;
    CirclePageIndicator catIndicator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_catalog, container, false);
        context = rootView.getContext();
        setHasOptionsMenu(true);
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50*1024*1024)
                .diskCacheSize(50*1024*1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


        catalogPager = (ViewPager) rootView.findViewById(R.id.catalog_pager);

        catalogs = new ArrayList<Catalog>();

        try
        {
            catalogs = getPreferences();
            Log.e("CLASS SERIALIZABLE TRAN","ERROR");
        }
        catch (Exception e)
        {
            Log.e("CLASS SERIALIZABLE TRAN","ERROR");
            if(catalogs.size()==0)
            {
                catalogs = getPreferences();
            }
        }

        catalogAdapter = new CatalogPagerAdapter(context, catalogs,imageLoader);
        catalogPager.setAdapter(catalogAdapter);

        if(catalogs.size()==1)
        {
            catalogPager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });
        }

        catIndicator = (CirclePageIndicator) rootView.findViewById(R.id.catalog_indicator);
        catIndicator.setViewPager(catalogPager);


        return rootView;
    }

    private ArrayList<Catalog> getPreferences()
    {
        ArrayList<Catalog> savedCatalogs;
        String catalogsJSONString = context.getSharedPreferences("catalog", context.MODE_PRIVATE).getString("catalogs", null);
        Type type = new TypeToken< ArrayList < Catalog >>() {}.getType();
        savedCatalogs = new Gson().fromJson(catalogsJSONString, type);
        return  savedCatalogs;
    }


}
