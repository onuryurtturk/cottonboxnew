package batunet.cottonboxnav.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import batunet.cottonboxnav.NewsDetailActivity;
import batunet.cottonboxnav.R;
import batunet.cottonboxnav.adapters.NewsGridAdapter;
import batunet.cottonboxnav.models.News;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 21.5.2015.
 */
public class FragmentPress extends Fragment {


    private View view = null;
    Context context;
    private static ArrayList<News> press;
    private NewsGridAdapter adapter;
    private GridView pressGrid;
    NetworkOperations operations;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    TextView actionText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {

            view = inflater.inflate(R.layout.fragment_media, container, false);
            context = view.getContext();
            press = new ArrayList<>();
            pressGrid = (GridView) view.findViewById(R.id.media_list);
            operations = new NetworkOperations(context);


            if (operations.isNetworkConnected()) {
                new jsonPressTask().execute();
            } else {

                SharedPrefUtils utils = new SharedPrefUtils(context);
                press = utils.getPrefPress();
                adapter = new NewsGridAdapter(context, press);
                pressGrid.setAdapter(adapter);

            }

            pressGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    News newsItem = press.get(position);
                    Intent intent = new Intent(context, NewsDetailActivity.class);
                    intent.putExtra("original_url", newsItem.getOriginal_image_url());
                    intent.putExtra("detail_text", newsItem.getDetail());
                    context.startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                }
            });

            initActionBar();



        } catch (InflateException e) {
            Log.e("InflaterProblem", "FragmentTv");

        }

        return view;
    }


    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        View view = getActivity().getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView) view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(getResources().getString(R.string.menu_press));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private class jsonPressTask extends AsyncTask<String, String, JSONArray> {

        JsonParser jParser;
        JSONObject mediaItem, mediaObject;
        JSONArray mediaJsonArray;
        ProgressDialog pDialog;


        @Override
        protected void onPreExecute() {

            pDialog = null;
            pDialog = new ProgressDialog(context);
            pDialog.setMessage(getResources().getString(R.string.dialog_message));
            pDialog.setTitle(getResources().getString(R.string.dialog_title));
            pDialog.setCancelable(false);
            pDialog.show();
        }


        @Override
        protected JSONArray doInBackground(String... params) {

            jParser = new JsonParser();
            mediaObject = jParser.getJSONObjectFromUrl(Constants.MEDIA_URL);

            try {
                mediaJsonArray = ((JSONArray) mediaObject.get("press"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            News newsItem;


            try {
                for (int i = 0; i < mediaJsonArray.length(); i++) {

                    newsItem = new News();
                    mediaItem = (JSONObject) mediaJsonArray.get(i);
                    newsItem.setId(Integer.parseInt(mediaItem.get("id").toString()));
                    newsItem.setTitle(mediaItem.get("title").toString());
                    newsItem.setThumb_image_url(Constants.MISSING_URL + mediaItem.get("thumb_image_url").toString());
                    newsItem.setOriginal_image_url(Constants.MISSING_URL + mediaItem.get("original_image_url").toString());
                    newsItem.setDetail(mediaItem.get("detail").toString());
                    newsItem.setCreated_at(mediaItem.get("created_at").toString());
                    if (Integer.parseInt(mediaItem.get("is_news").toString()) == 0) {
                        newsItem.setIs_news(false);

                    } else if (Integer.parseInt(mediaItem.get("is_news").toString()) == 1) {
                        newsItem.setIs_news(true);
                    }

                    press.add(newsItem);


                }

            } catch (Exception e) {
                Log.e("ASNYCTASK", "DoInBackgroundError");
            }

            return mediaJsonArray;
        }


        @Override
        protected void onPostExecute(JSONArray jsonArray) {

            pDialog.dismiss();
            SharedPrefUtils utils = new SharedPrefUtils(context);
            utils.savePrefPress(press);
            adapter = new NewsGridAdapter(context, press);
            pressGrid.setAdapter(adapter);
            //cacheAllImages();


        }


    }

}
