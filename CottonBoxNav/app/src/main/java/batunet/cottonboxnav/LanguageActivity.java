package batunet.cottonboxnav;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;


import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import batunet.cottonboxnav.models.Language;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 14.7.2015.
 */
public class LanguageActivity extends ActionBarActivity {


    Context context;
    LinearLayout layout_language;
    Button btn_continue;
    Spinner language_spinner;
    static ArrayList<Language> languageList;
    NetworkOperations network;
    AlertDialog.Builder alert;
    SharedPrefUtils utils;

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        android.os.Process.killProcess(android.os.Process.myPid());
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        context = this;
        languageList = new ArrayList<>();
        btn_continue = (Button) findViewById(R.id.btn_language);
        layout_language = (LinearLayout) findViewById(R.id.linear_language);
        layout_language.setBackgroundResource(R.drawable.shape);
        language_spinner = (Spinner) findViewById(R.id.language_spinner);
        network = new NetworkOperations(context);
        alert = new AlertDialog.Builder(context);
        utils = new SharedPrefUtils(context);

        if (network.isNetworkConnected()) {
            new jsonLanguageParse().execute();
            //getLanguages();
        } else
        {
            Toast.makeText(context, getResources().getString(R.string.language_warning_net), Toast.LENGTH_LONG).show();
            if(utils.getFirstRun()==1)
            {

                alert.setTitle(getResources().getString(R.string.language_net_problem_title));
                alert.setMessage(getResources().getString(R.string.language_net_problem));
                alert.setCancelable(false);

                alert.setPositiveButton(getResources().getString(R.string.map_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Language offlineMode = ((ArrayList<Language>)utils.getPrefLanguage()).get(0);
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("language_id", offlineMode.getLanguage_id());
                        returnIntent.putExtra("language_name", offlineMode.getLanguage_title());
                        returnIntent.putExtra("language_shortname", offlineMode.getLanguage_short());
                        setResult(RESULT_OK, returnIntent);
                        finish();

                    }
                });
                alert.setNegativeButton(getResources().getString(R.string.cache_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        killApp();
                    }
                });
                alert.show();
            }
            else
            {
                killApp();
            }
        }
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (language_spinner.getSelectedItem() != null) {

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("language_id", ((Language) language_spinner.getSelectedItem()).getLanguage_id());
                    returnIntent.putExtra("language_name", ((Language) language_spinner.getSelectedItem()).getLanguage_title());
                    returnIntent.putExtra("language_shortname", ((Language) language_spinner.getSelectedItem()).getLanguage_short());

                    setResult(RESULT_OK, returnIntent);
                    finish();
                }

            }
        });


    }
    private void killApp() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                android.os.Process.killProcess(android.os.Process.myPid());
                finish();
            }
        }, 7000);
    }


    private void getLanguages()
    {

        RestClient.get("languages", null, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Log.e("Responsefail","This" + responseString);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                Log.e("Response","This" + responseString);
            }
        });

    }

    private class jsonLanguageParse extends AsyncTask<String, String, JSONArray> {

        JsonParser jParser;
        JSONObject languageItem;
        JSONArray languageJsonArray;


        @Override
        protected JSONArray doInBackground(String... params) {

            jParser = new JsonParser();
            languageJsonArray = jParser.getJSONArrayFromUrl(Constants.LANGUAGE_URL);
            Language language;


            try {
                for (int i = 0; i < languageJsonArray.length(); i++) {
                    languageItem = (JSONObject) languageJsonArray.get(i);
                    language = new Language();
                    language.setLanguage_id((Integer) languageItem.get("id"));
                    language.setLanguage_title(languageItem.getString("name"));
                    language.setLanguage_short(languageItem.getString("shortname"));
                    languageList.add(language);
                }

            } catch (Exception e) {
                Log.e("ASNYCTASK", "DoInBackgroundError");
            }

            return languageJsonArray;
        }


        @Override
        protected void onPostExecute(JSONArray jsonArray) {

            ArrayAdapter<Language> languageAdapter = new ArrayAdapter<Language>(context, R.layout.spinner, languageList);
            languageAdapter.setDropDownViewResource(R.layout.spinner);
            language_spinner.setAdapter(languageAdapter);

        }

    }


}
