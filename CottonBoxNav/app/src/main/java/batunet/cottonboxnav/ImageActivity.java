package batunet.cottonboxnav;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import batunet.cottonboxnav.fragments.ImageGalleryFragmentBase;
import batunet.cottonboxnav.fragments.ImagePagerFragmentBase;
import batunet.cottonboxnav.models.Pages;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;

/**
 * Created by onuryurtturk on 4/20/2015.
 */
public class ImageActivity extends ActionBarActivity {

    int frIndex;
    Context context;
    String catalog_id, page_count;
    private static String url = "http://dev.mobilkatalog.net/api/v1/catalogs";
    private static String full_url;
    public static ArrayList<Pages> catPages;
    public static ArrayList<Catalog> savedCatalogs;
    SharedPreferences iPrefs;
    public ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    SharedPreferences mPrefs;
    Catalog currentCat;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        catalog_id = getIntent().getStringExtra("catalog_id");
        page_count = getIntent().getStringExtra("page_count");

        full_url = url + "/" + catalog_id;

        iPrefs = getSharedPreferences("page", MODE_PRIVATE);
        mPrefs = getSharedPreferences("catalog", MODE_PRIVATE);

        catPages = new ArrayList<Pages>();
        savedCatalogs = new ArrayList<Catalog>();
        savedCatalogs = getCatalogPreferences();
        currentCat = findCatalog(savedCatalogs);

        frIndex = getIntent().getIntExtra(Constants.Extra.FRAGMENT_INDEX, 0);


        if (isNetworkConnected()) //Network is connected download pages
        {
            if (frIndex == ImageGalleryFragmentBase.INDEX) {

                if ((checkFileExist(catalog_id) == false) && (currentCat.isDownloaded() == false)) {
                    new JSONParse().execute(full_url);

                } else {
                    Toast.makeText(context, "İndirilen katalog açılıyor", Toast.LENGTH_LONG).show();
                    catPages = getPreferences();
                    frIndex = getIntent().getIntExtra(Constants.Extra.FRAGMENT_INDEX, 0);
                    changeFragment(frIndex);
                }

            } else if (frIndex == ImagePagerFragmentBase.INDEX) {
                catPages = getPreferences();
                frIndex = getIntent().getIntExtra(Constants.Extra.FRAGMENT_INDEX, 0);
                changeFragment(frIndex);
            }
        } else {
            catPages = getPreferences();
            frIndex = getIntent().getIntExtra(Constants.Extra.FRAGMENT_INDEX, 0);
            changeFragment(frIndex);

        }
    }


    private Catalog findCatalog(ArrayList<Catalog> myCatalogs) {
        if (myCatalogs.size() != 0) {
            for (int i = 0; i < myCatalogs.size(); i++) {
                if (myCatalogs.get(i).getId().equals(catalog_id)) {
                    return myCatalogs.get(i);
                }
            }
        }
        return null;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Katalog indiriliyor.. Lütfen bekleyiniz..");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgress(0);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    @Override
    public void onBackPressed() {
        finish();

    }

    private void changeFragment(int frIndex) {
        Fragment fr = null;
        String tag = "";
        int titleRes;

        switch (frIndex) {
            default:
            case ImagePagerFragmentBase.INDEX:
                tag = ImagePagerFragmentBase.class.getSimpleName();
                fr = getSupportFragmentManager().findFragmentByTag(tag);
                if (fr == null) {
                    fr = new ImagePagerFragmentBase();
                    //fr.setArguments(getIntent().getExtras());
                    Bundle bundleObject = new Bundle();
                    bundleObject.putBundle("bundle", getIntent().getExtras());
                    bundleObject.putSerializable("catPages", catPages);
                    fr.setArguments(bundleObject);

                }
                titleRes = R.string.app_name;
                break;
            case ImageGalleryFragmentBase.INDEX:
                tag = ImageGalleryFragmentBase.class.getSimpleName();
                fr = getSupportFragmentManager().findFragmentByTag(tag);
                if (fr == null) {
                    fr = new ImageGalleryFragmentBase();
                    Bundle bundleObject = new Bundle();
                    bundleObject.putString("catalog_id", catalog_id);
                    bundleObject.putSerializable("catPages", catPages);
                    fr.setArguments(bundleObject);
                }
                titleRes = R.string.app_name;
                break;
        }
        setTitle(titleRes);
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, fr, tag).addToBackStack("fragment").commit();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }

    private void savePreferences(ArrayList<Pages> cPages) {

        SharedPreferences.Editor prefsEditor = iPrefs.edit();
        prefsEditor.clear();
        Gson gson = new Gson();
        String json = gson.toJson(cPages);
        prefsEditor.putString("pages", json);
        prefsEditor.commit();
    }

    private ArrayList<Pages> getPreferences() {
        ArrayList<Pages> savedPages = new ArrayList<Pages>();
        String catalogsJSONString = context.getSharedPreferences("page", MODE_PRIVATE).getString("pages", null);
        Type type = new TypeToken<ArrayList<Pages>>() {
        }.getType();
        savedPages = new Gson().fromJson(catalogsJSONString, type);


        return savedPages;
    }

    private void saveCatalogPreferences(ArrayList<Catalog> catalogs) {
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.remove("catalogs");
        prefsEditor.clear().commit();
        Gson gson = new Gson();
        String json = gson.toJson(catalogs);
        prefsEditor.putString("catalogs", json);
        prefsEditor.commit();


    }

    private ArrayList<Catalog> getCatalogPreferences() {
        ArrayList<Catalog> savedCatalogs;
        String catalogsJSONString = context.getSharedPreferences("catalog", MODE_PRIVATE).getString("catalogs", null);
        Type type = new TypeToken<ArrayList<Catalog>>() {
        }.getType();
        savedCatalogs = new Gson().fromJson(catalogsJSONString, type);
        return savedCatalogs;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.home:
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {

    }


    public boolean checkFileExist(String catalog_id) {
        File check = new File(Environment.getExternalStorageDirectory()
                + "/Cotton/" + catalog_id);


        if (check.isDirectory()) {
            int catSize = Integer.parseInt(page_count);
            if ((check.list()).length >= catSize) {
                return true;
            }

        }

        return false;
    }

    public String downloadFile(String uRl, String filename, String catalogId) {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/Cotton/" + catalogId);

        if (!direct.exists()) {
            direct.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(true)
                .setDestinationInExternalPublicDir("/Cotton/" + catalogId, filename)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
        mgr.enqueue(request);


        return Environment.getExternalStorageDirectory() + "/Cotton/" + catalogId + "/" + filename;
    }




    private class JSONParse extends AsyncTask<String, String, JSONObject> {

        JsonParser jParser;
        JSONObject obj, catalog = null;
        int total=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
           /*registerReceiver(onComplete,
                   new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));*/



        }

        @Override
        protected JSONObject doInBackground(String... args) {

            String json_url = args[0];
            jParser = new JsonParser();
            JSONObject json = jParser.getJSONFromUrl(json_url);
            String localUrl;

            try {

                obj = json.getJSONObject("result");
                catalog = obj.getJSONObject("catalog");

                JSONArray pages;

                pages = catalog.getJSONArray("pages");
                for (int i = 0; i < pages.length(); i++) {
                    Pages page = new Pages();
                    JSONObject object = pages.getJSONObject(i);
                    page.setId(object.getString("id"));
                    page.setCatalog_id(object.getString("catalog_id"));
                    page.setPhoto_file_name(object.getString("photo_file_name"));
                    page.setPhoto_file_size(object.getString("photo_file_size"));
                    page.setPhoto_medium_url(object.getString("photo_iphone4s_url"));
                    page.setPhoto_original_url(object.getString("photo_original_url"));
                    catPages.add(page);
                }

                if (!(catPages.size() == 0)) {

                    File direct = new File(Environment.getExternalStorageDirectory()
                            + "/Cotton/" + catalog_id);

                    if (!direct.exists()) {
                        direct.mkdirs();
                    }

                    URL url = null;
                    int fileLength;

                    for (int i = 0; i < catPages.size(); i++) {

                        try {
                            url = new URL(catPages.get(i).getPhoto_original_url());
                            URLConnection connection = url.openConnection();
                            connection.connect();
                            fileLength = connection.getContentLength();
                            InputStream input = new BufferedInputStream(url.openStream());
                            OutputStream output = new FileOutputStream(direct + "/" + catPages.get(i).getPhoto_file_name());

                            byte data[] = new byte[1024];
                            long total = 0;
                            int count;
                            while ((count = input.read(data)) != -1) {
                                total += count;
                                //publishProgress("" + (int) (total * 100 / fileLength));
                                output.write(data, 0, count);
                            }

                            output.flush();
                            output.close();
                            input.close();


                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        catPages.get(i).setPhoto_original_url(direct + "/" + catPages.get(i).getPhoto_file_name());
                        catPages.get(i).setPhoto_medium_url(direct + "/" + catPages.get(i).getPhoto_file_name());
                         publishProgress("" + (int) ((i * 100) / catPages.size()));

                    }


                        currentCat.setDownloaded(true);
                }

                if (savedCatalogs.size() != 0) {
                    for (int j = 0; j < savedCatalogs.size(); j++) {
                        if (savedCatalogs.get(j).getId().equals(catalog_id)) {
                            savedCatalogs.get(j).setDownloaded(true);
                        }
                    }
                }


                Log.e("Download Durumu Geldi", savedCatalogs.get(0).isDownloaded().toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return catalog;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            pDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(JSONObject json) {

            Log.e("First Page of Catalog", catPages.get(0).getPhoto_file_name());
            savePreferences(catPages);
            saveCatalogPreferences(savedCatalogs);
            dismissDialog(progress_bar_type);
            frIndex = getIntent().getIntExtra(Constants.Extra.FRAGMENT_INDEX, 0);
            changeFragment(frIndex);

        }

    }


}


