package batunet.cottonboxnav;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import batunet.cottonboxnav.fragments.ImageGalleryFragmentBase;
import batunet.cottonboxnav.models.Pages;
import batunet.cottonboxnav.utils.Constants;

/**
 * Created by onuryurtturk on 4/18/2015.
 */
public class CatalogPagerAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<Catalog> catalogList;
    LayoutInflater inflater;
    String cat_id;

    private static final String TAG = ImageActivity.class.getSimpleName();
    private static String url = "http://dev.mobilkatalog.net/api/v1/catalogs";
    private static String full_url;
    public List<Pages> catPages;
    DisplayImageOptions options;
    private ImageLoader imageLoader;



    public CatalogPagerAdapter(Context context, ArrayList<Catalog> catalogList, ImageLoader imageloader) {
        this.context = context;
        this.catalogList = catalogList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imageLoader=imageloader;

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50*1024*1024)
                .diskCacheSize(50*1024*1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

    }



    @Override
    public int getCount() {
        return catalogList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == ((LinearLayout) o);
    }

    @Override
    public float getPageWidth(int position) {
        return 1f;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View item = inflater.inflate(R.layout.item_catalog, container, false);

        catPages = new ArrayList<Pages>();

        ImageView thumb = (ImageView) item.findViewById(R.id.cat_image);

        Catalog cat=catalogList.get(position);


        TextView catTitle = (TextView)item.findViewById(R.id.cat_title);
        catTitle.setText(catalogList.get(position).getTitle());
        final String catalog_id=cat.getId();
        cat_id=cat.getId();




        final String page_count = cat.pages_count;
        Log.e("CATALOG ID", String.valueOf(cat.getId()));
        full_url = url + "/" + cat_id;

        String uri = (cat.getCover_original_url());
        String decodedUri = Uri.decode(uri);
        String fulldecoded = "file://" +decodedUri;


        imageLoader.displayImage(fulldecoded, thumb, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                Log.e("Hata","ERRORR");
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                Log.e("Hata", "ERRORRFAILED");

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
            }
        });


        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImageActivity.class);
                intent.putExtra(Constants.Extra.FRAGMENT_INDEX, ImageGalleryFragmentBase.INDEX);
                intent.putExtra("catalog_id",catalog_id);
                intent.putExtra("page_count",page_count);
                context.startActivity(intent);
            }
        });

        container.addView(item);
        return item;
    }




    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }








}
