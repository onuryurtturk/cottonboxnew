package batunet.cottonboxnav.models;

/**
 * Created by onur on 12.6.2015.
 */
public class Contact {

    private int icon;
    private String title;
    private boolean isGroupHeader = false;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isGroupHeader() {
        return isGroupHeader;
    }

    public void setIsGroupHeader(boolean isGroupHeader) {
        this.isGroupHeader = isGroupHeader;
    }

    public Contact(String title) {
        this(-1, title);
        isGroupHeader = true;
    }

    public Contact(int icon, String title) {
        super();
        this.icon = icon;
        this.title = title;
    }
}
