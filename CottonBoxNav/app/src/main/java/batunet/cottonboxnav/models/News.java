package batunet.cottonboxnav.models;

import java.io.Serializable;

/**
 * Created by onuryurtturk on 5/15/2015.
 */
public class News implements Serializable {

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getThumb_image_url() {
        return thumb_image_url;
    }

    public void setThumb_image_url(String thumb_image_url) {
        this.thumb_image_url = thumb_image_url;
    }

    public String getOriginal_image_url() {
        return original_image_url;
    }

    public void setOriginal_image_url(String original_image_url) {
        this.original_image_url = original_image_url;
    }

    public boolean getIs_news() {
        return is_news;
    }

    public void setIs_news(boolean is_news) {
        this.is_news = is_news;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Integer id;
    public String title;
    public String detail;
    public String thumb_image_url;
    public String original_image_url;
    public boolean is_news;
    public String created_at;
}
