package batunet.cottonboxnav.models;

import java.io.Serializable;

/**
 * Created by onur on 9.7.2015.
 */
public class Slider implements Serializable {

    public String url;
    public String text;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


}
