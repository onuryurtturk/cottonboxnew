package batunet.cottonboxnav.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by onur on 28.7.2015.
 */
public class CatalogF implements Serializable {


    public ArrayList<String> pages;
    public ArrayList<String> original_pages;
    public int page_count;
    public String cover_page;
    public String name;
    public boolean is_downloaded;

    public CatalogF() {

        is_downloaded = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getOriginal_pages() {
        return original_pages;
    }

    public void setOriginal_pages(ArrayList<String> original_pages) {
        this.original_pages = original_pages;
    }


    public ArrayList<String> getPages() {
        return pages;
    }

    public void setPages(ArrayList<String> pages) {
        this.pages = pages;
    }

    public int getPage_count() {
        return page_count;
    }

    public void setPage_count(int page_count) {
        this.page_count = page_count;
    }


    public String getCover_page() {
        return cover_page;
    }

    public void setCover_page(String cover_page) {
        this.cover_page = cover_page;
    }

    public boolean is_downloaded() {
        return is_downloaded;
    }

    public void setIs_downloaded(boolean is_downloaded) {
        this.is_downloaded = is_downloaded;
    }
}
