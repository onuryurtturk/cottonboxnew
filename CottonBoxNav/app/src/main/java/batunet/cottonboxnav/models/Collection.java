package batunet.cottonboxnav.models;

import java.io.Serializable;

/**
 * Created by onuryurtturk on 5/14/2015.
 */
public class Collection implements Serializable {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String id;
    private String title;
    private String url;
}
