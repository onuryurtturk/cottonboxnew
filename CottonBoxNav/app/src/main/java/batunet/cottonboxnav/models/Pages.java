package batunet.cottonboxnav.models;

import java.io.Serializable;

/**
 * Created by onuryurtturk on 4/30/2015.
 */
public class Pages implements Serializable{


    String id;
    String catalog_id;
    String photo_file_name;
    String photo_file_size;
    String photo_content_type;
    String photo_medium_url;
    String photo_thumb_url;
    String photo_mini_url;
    String photo_original_url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatalog_id() {
        return catalog_id;
    }

    public void setCatalog_id(String catalog_id) {
        this.catalog_id = catalog_id;
    }

    public String getPhoto_file_name() {
        return photo_file_name;
    }

    public void setPhoto_file_name(String photo_file_name) {
        this.photo_file_name = photo_file_name;
    }

    public String getPhoto_file_size() {
        return photo_file_size;
    }

    public void setPhoto_file_size(String photo_file_size) {
        this.photo_file_size = photo_file_size;
    }

    public String getPhoto_content_type() {
        return photo_content_type;
    }

    public void setPhoto_content_type(String photo_content_type) {
        this.photo_content_type = photo_content_type;
    }

    public String getPhoto_medium_url() {
        return photo_medium_url;
    }

    public void setPhoto_medium_url(String photo_medium_url) {
        this.photo_medium_url = photo_medium_url;
    }

    public String getPhoto_thumb_url() {
        return photo_thumb_url;
    }

    public void setPhoto_thumb_url(String photo_thumb_url) {
        this.photo_thumb_url = photo_thumb_url;
    }

    public String getPhoto_mini_url() {
        return photo_mini_url;
    }

    public void setPhoto_mini_url(String photo_mini_url) {
        this.photo_mini_url = photo_mini_url;
    }

    public String getPhoto_original_url() {
        return photo_original_url;
    }

    public void setPhoto_original_url(String photo_original_url) {
        this.photo_original_url = photo_original_url;
    }
}
