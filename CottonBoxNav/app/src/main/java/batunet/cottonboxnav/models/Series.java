package batunet.cottonboxnav.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by onur on 19.5.2015.
 */
public class Series implements Serializable {


    public int id;
    public String name;
    public String thumb_image_url;
    public String original_image_url;
    public String description;
    public int channel_id;
    public ArrayList<String> images;
    public String jsonImages;

    public String getImage_details() {
        return image_details;
    }

    public void setImage_details(String image_details) {
        this.image_details = image_details;
    }

    public String image_details;


    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJsonImages() {
        return jsonImages;
    }

    public void setJsonImages(String jsonImages) {
        this.jsonImages = jsonImages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumb_image_url() {
        return thumb_image_url;
    }

    public void setThumb_image_url(String thumb_image_url) {
        this.thumb_image_url = thumb_image_url;
    }

    public String getOriginal_image_url() {
        return original_image_url;
    }

    public void setOriginal_image_url(String original_image_url) {
        this.original_image_url = original_image_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(int channel_id) {
        this.channel_id = channel_id;
    }


}
