package batunet.cottonboxnav.models;

import java.io.Serializable;

/**
 * Created by onuryurtturk on 5/15/2015.
 */
public class Product implements Serializable {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getThumb_image_url() {
        return thumb_image_url;
    }

    public void setThumb_image_url(String thumb_image_url) {
        this.thumb_image_url = thumb_image_url;
    }

    public String getOriginal_image_url() {
        return original_image_url;
    }

    public void setOriginal_image_url(String original_image_url) {
        this.original_image_url = original_image_url;
    }

    public String getCollection_id() {
        return collection_id;
    }

    public void setCollection_id(String collection_id) {
        this.collection_id = collection_id;
    }

    public String id;
    public String title;
    public String detail;
    public String thumb_image_url;
    public String original_image_url;
    public String collection_id;

}
