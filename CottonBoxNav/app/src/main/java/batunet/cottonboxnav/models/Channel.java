package batunet.cottonboxnav.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by onur on 19.5.2015.
 */
public class Channel implements Serializable {


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public ArrayList<Series> getSeriesList() {
        return seriesList;
    }

    public void setSeriesList(ArrayList<Series> seriesList) {
        this.seriesList = seriesList;
    }

    public  int id;
    public String name;
    public String image_url;
    public ArrayList<Series> seriesList;
}
