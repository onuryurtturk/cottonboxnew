package batunet.cottonboxnav.models;

import java.util.ArrayList;

/**
 * Created by onuryurtturk on 3/23/2015.
 */
public class DrawerItemModel {

    private String title;
    private int image;
    private String count="0";
    private boolean isVisible=false;
    private ArrayList<Object> childs;

    public ArrayList<Object> getChilds() {
        return childs;
    }

    public void setChilds(ArrayList<Object> childs) {
        this.childs = childs;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public DrawerItemModel()
    {}

    public DrawerItemModel(String title,int image)
    {
        this.title=title;
        this.image=image;
    }

}
