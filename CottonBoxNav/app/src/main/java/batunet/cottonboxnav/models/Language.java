package batunet.cottonboxnav.models;

/**
 * Created by onur on 14.7.2015.
 */
public class Language {


    public int language_id;
    public String language_title;
    public String language_short;

    public String getLanguage_short() {
        return language_short;
    }

    public void setLanguage_short(String language_short) {
        this.language_short = language_short;
    }

    public Language() {
    }

    public Language(int language_id, String language_title,String short_name) {
        this.language_id = language_id;
        this.language_title = language_title;
        this.language_short = short_name;
    }

    public int getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(int language_id) {
        this.language_id = language_id;
    }

    public String getLanguage_title() {
        return language_title;
    }

    public void setLanguage_title(String language_title) {
        this.language_title = language_title;
    }

    @Override
    public String toString() {
        return this.language_title;
    }

}
