package batunet.cottonboxnav;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import batunet.cottonboxnav.adapters.SetsGridAdapter;
import batunet.cottonboxnav.models.Sets;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;

/**
 * Created by onur on 13.6.2015.
 */
public class SetsActivity extends ActionBarActivity {

    Context context;
    private GridView setsGrid;
    private SetsGridAdapter setsGridAdapter;
    private String product_id,product_name;
    public static ArrayList<Sets> sets;
    NetworkOperations operations;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    TextView actionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sets);
        context = this;


        operations = new NetworkOperations(context);
        product_id = getIntent().getStringExtra("product_id");
        product_name = getIntent().getStringExtra("product_name");
        sets = new ArrayList<Sets>();
        setsGrid = (GridView) findViewById(R.id.sets_grid);


        if (operations.isNetworkConnected()) {
            //new getSets().execute(setsUrl);
            sets = getOfflineData(product_id);
            setsGridAdapter = new SetsGridAdapter(context, sets);
            setsGrid.setAdapter(setsGridAdapter);

        } else {
            sets = getOfflineData(product_id);
            setsGridAdapter = new SetsGridAdapter(context, sets);
            setsGrid.setAdapter(setsGridAdapter);

        }

        setsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra("original_url", sets.get(position).getImage());
                intent.putExtra("detail_name", sets.get(position).getTitle());
                intent.putExtra("product_name", product_name);
                intent.putExtra("position",position);
                intent.putExtra("set_list",sets);
                context.startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });

        initActionBar();
    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        View view =getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView)view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        actionText.setText(product_name);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    private void cacheAllImages() {
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(10)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        SharedPrefUtils utils = new SharedPrefUtils(context);
        ArrayList<Sets> mysets = utils.getPrefSet();

        for (int k = mysets.size()/2; k < mysets.size(); k++) {
            imageLoader.loadImage((mysets.get(k)).getImage(), new SimpleImageLoadingListener());

        }
    }

    private ArrayList<Sets> getOfflineData(String product_id) {
        ArrayList<Sets> allSets = new ArrayList<Sets>();
        ArrayList<Sets> offlineSets = new ArrayList<Sets>();
        SharedPrefUtils utils = new SharedPrefUtils(context);
        allSets = utils.getPrefSet();

        for (int i = 0; i < allSets.size(); i++) {
            if ((allSets.get(i)).getSet_id().equals(product_id)) {
                offlineSets.add(allSets.get(i));
            }

        }
        return offlineSets;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
           return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
