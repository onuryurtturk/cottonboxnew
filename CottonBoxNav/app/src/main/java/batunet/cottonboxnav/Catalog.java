package batunet.cottonboxnav;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by onuryurtturk on 4/28/2015.
 */
public class Catalog implements Serializable {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTotal_size() {
        return total_size;
    }

    public void setTotal_size(String total_size) {
        this.total_size = total_size;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover_thumb_url() {
        return cover_thumb_url;
    }

    public void setCover_thumb_url(String cover_thumb_url) {
        this.cover_thumb_url = cover_thumb_url;
    }

    public String getCover_medium_url() {
        return cover_medium_url;
    }

    public void setCover_medium_url(String cover_medium_url) {
        this.cover_medium_url = cover_medium_url;
    }

    public String getCover_mini_url() {
        return cover_mini_url;
    }

    public void setCover_mini_url(String cover_mini_url) {
        this.cover_mini_url = cover_mini_url;
    }

    public String getCover_original_url() {
        return cover_original_url;
    }

    public void setCover_original_url(String cover_original_url) {
        this.cover_original_url = cover_original_url;
    }

    public String getPages_count() {
        return pages_count;
    }

    public void setPages_count(String pages_count) {
        this.pages_count = pages_count;
    }





    public String getCover_file_name() {
        return cover_file_name;
    }

    public void setCover_file_name(String cover_file_name) {
        this.cover_file_name = cover_file_name;
    }

    public String getCover_file_size() {
        return cover_file_size;
    }

    public void setCover_file_size(String cover_file_size) {
        this.cover_file_size = cover_file_size;
    }

    public String getCover_content_type() {
        return cover_content_type;
    }

    public void setCover_content_type(String cover_content_type) {
        this.cover_content_type = cover_content_type;
    }

    public String getCover_updated_at() {
        return cover_updated_at;
    }

    public void setCover_updated_at(String cover_updated_at) {
        this.cover_updated_at = cover_updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


    String id;
    String title;
    String cover_file_name;
    String cover_file_size;
    String cover_content_type;
    String cover_updated_at;
    String created_at;
    String updated_at;
    String cover_medium_url;
    String cover_thumb_url;
    String cover_mini_url;
    String cover_original_url;
    String pages_count;
    String total_size;
    Boolean downloaded;

    public Catalog()
    {
        this.downloaded=false;
    }


    public Boolean isDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Boolean downloaded) {
        this.downloaded = downloaded;
    }



    public HashMap<String, String> getMap() {
        HashMap<String, String> myCatalogs = new HashMap<String, String>();


        return null;
    }
}
