package batunet.cottonboxnav;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

/**
 * Created by onur on 2.5.2015.
 */
public class FavActivity extends ActionBarActivity {

    Context context;
    ImageView favorite_view;
    String share_url=null;
    public Bitmap share_Image;
    BitmapDrawable drawable;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.action_share :
            {
                View dialog = getLayoutInflater().inflate(R.layout.layout_share_dialog, null);
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                        .setView(dialog)
                        .setTitle(getResources().getString(R.string.share_title))
                        .setCancelable(true);

                ImageView image = (ImageView) dialog.findViewById(R.id.share_dialog_image);
                image.setImageBitmap(share_Image);

                final EditText message = (EditText) dialog.findViewById(R.id.share_dialog_message);


                alertDialog.setPositiveButton(getResources().getString(R.string.app_button_okay),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(message.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                                shareImage(message.getText().toString(), share_url);
                            }
                        });

                alertDialog.setNegativeButton(getResources().getString(R.string.share_button_cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                               dialogInterface.dismiss();
                            }
                        });

                        alertDialog.show();
            }
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    String photo_url;
    DisplayImageOptions options;
    private ImageLoader imageLoader;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.social_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        context = this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .memoryCacheSize(50 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .threadPoolSize(5)
                .writeDebugLogs()
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


        photo_url = getIntent().getStringExtra("fav_image");
        favorite_view = (ImageView) findViewById(R.id.favorite_image);
        String decodedUri = Uri.decode(photo_url);


        imageLoader.displayImage("file://" + decodedUri, favorite_view, options, new SimpleImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                //Toast.makeText(view.getContext(), "Lütfen İnternet Bağlantınızı Kontrol edin!", Toast.LENGTH_SHORT).show();
                favorite_view.setImageDrawable(getResources().getDrawable(R.drawable.ic_no_photo));


            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                try
                {
                    drawable = (BitmapDrawable)favorite_view.getDrawable();
                    share_Image = drawable.getBitmap();
                    share_url=saveBitmap(share_Image);
                }
                catch (Exception e)
                {

                    share_Image= BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.img_cottonp);
                    share_url=saveBitmap(share_Image);


                }
            }
        });

        if(share_Image==null)
        {
            share_Image= BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.img_cottonp);
            share_url=saveBitmap(share_Image);

        }

        //share_Image=(Bitmap)getIntent().getParcelableExtra("share_bitmap");
        //share_Image=imageLoader.getMemoryCache().get(photo_url);
        //String S=imageLoader.getDiskCache().get(photo_url).toString();
        //Toast.makeText(this,S,Toast.LENGTH_LONG).show();

    }

    private void shareImage(String message,String url)
    {

        Intent imageIntent = new Intent(Intent.ACTION_SEND);
        imageIntent.setType("*/*");
        imageIntent.putExtra(Intent.EXTRA_TEXT, "Cotton Box Android App -> " + message);
        imageIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(url)));
        imageIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        imageIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivity(imageIntent);

    }

    private String saveBitmap(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Cotton/Favorites/");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Favorites-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return myDir.getAbsolutePath()+"/"+ fname;

    }
}
