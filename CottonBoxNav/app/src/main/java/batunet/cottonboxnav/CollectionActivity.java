package batunet.cottonboxnav;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import batunet.cottonboxnav.adapters.CategoryGridAdapter;
import batunet.cottonboxnav.adapters.SliderAdapter;
import batunet.cottonboxnav.models.Channel;
import batunet.cottonboxnav.models.Collection;
import batunet.cottonboxnav.models.Language;
import batunet.cottonboxnav.models.News;
import batunet.cottonboxnav.models.Product;
import batunet.cottonboxnav.models.Series;
import batunet.cottonboxnav.models.Sets;
import batunet.cottonboxnav.models.Slider;
import batunet.cottonboxnav.utils.Constants;
import batunet.cottonboxnav.utils.JsonParser;
import batunet.cottonboxnav.utils.NetworkOperations;
import batunet.cottonboxnav.utils.SharedPrefUtils;


public class CollectionActivity extends ActionBarActivity {

    Context context;
    private GridView categoriesGrid;
    private CategoryGridAdapter categoryGridAdapter;
    private ViewPager slider;
    private SliderAdapter sliderAdapter;

    public static ArrayList<Collection> collections;
    int currentPage = 0;
    Timer swipeTimer;
    SharedPreferences prefSlider, prefCollection;
    public static ArrayList<Collection> allCollection;
    public static ArrayList<Product> allProduct;
    public static ArrayList<Sets> allSets;
    public static ArrayList<News> allNews, allPress;
    public static ArrayList<Channel> allChannel;
    public ArrayList<Language> languages;
    private ArrayList<Slider> sliderImages;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    NetworkOperations operations;
    CirclePageIndicator catIndicator;
    AlertDialog.Builder builder;
    AlertDialog alert;
    SharedPrefUtils utils;
    Language appLanguage;
    TextView actionText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        //slider = (ViewPager) findViewById(R.id.slider_pager);
        categoriesGrid = (GridView) findViewById(R.id.category_grid);

        operations = new NetworkOperations(context);

        prefSlider = getSharedPreferences("slider", MODE_PRIVATE);
        prefCollection = getSharedPreferences("collection", MODE_PRIVATE);

        initArrayList(context);
        initActionBar();


        if (operations.isNetworkConnected() == true) {


//            Log.e("UTILS", "FIRST RUN " + String.valueOf(utils.getFirstRun()));
//            Log.e("UTILS","OFFLINE " + String.valueOf(utils.getOfflineMode()));
//
//            if(utils.getFirstRun()==0)
//            {
//                utils.saveFirstRun(1);
//                cacheAllImages();
//            }
//            else if(utils.getOfflineMode()==1)
//            {
//                cacheAllImages();
//            }
            //cacheAllImages();

            if (utils.getFirstRun() == 0) {
                utils.saveFirstRun(1);
                cacheAllImages();
            } else if (utils.getOfflineMode() == 1) {
                cacheAllImages();
            }
            new JsonSliderParse().execute(Constants.SLIDER_URL);


        } else {

            showAlert(context);
            SharedPrefUtils utils = new SharedPrefUtils(context);
            collections = utils.getPrefCollection();
           /* sliderImages = utils.getPrefSlider();
            sliderAdapter = new SliderAdapter(context, sliderImages);
            slider.setAdapter(sliderAdapter);*/
            categoryGridAdapter = new CategoryGridAdapter(context, collections);
            categoriesGrid.setAdapter(categoryGridAdapter);
            disMissAlert();

        }
    }

    private AlertDialog showAlert(Context context) {
        View dialoglayout = getLayoutInflater().inflate(R.layout.custom_alert_dialog, null);
        builder = new AlertDialog.Builder(context);
        builder.setView(dialoglayout);
        builder.setCancelable(false);
        alert = builder.create();
        alert.show();

        return alert;
    }

    private void disMissAlert() {

        if (alert != null) {
            alert.dismiss();
        }
    }

    private void initArrayList(Context context) {
        utils = new SharedPrefUtils(context);
        sliderImages = new ArrayList<>();

        //initialise arraylists
        languages = new ArrayList<>();
        sliderImages = new ArrayList();
        collections = new ArrayList<>();
        allCollection = new ArrayList<>();


        try {
            languages = utils.getPrefLanguage();
            allProduct = utils.getPrefProduct();
            allSets = utils.getPrefSet();
            allNews = utils.getPrefNews();
            allChannel = utils.getPrefChannel();
            allPress = utils.getPrefPress();
            collections = utils.getPrefCollection();
            appLanguage = languages.get(0);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("ARRAYLIST", "error");
        }

    }

    private void initActionBar() {

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        View view = getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null);
        actionText = ((TextView) view.findViewById(R.id.actionbar_textview));
        actionBar.setCustomView(view);
        // actionBar.setCustomView(getLayoutInflater().inflate(R.layout.custom_actionbar_layout, null));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    private void cacheAllImages() {

        options = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.ic_menu)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheSize(150 * 1024 * 1024)
                .threadPoolSize(1)
                .writeDebugLogs()
                .build();


        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


        new cacheMedia().execute();
        for (int j = 0; j < allProduct.size(); j++) {

            Log.e("CACHEPRODUCTS " + String.valueOf(j), (allProduct.get(j)).getOriginal_image_url());
            imageLoader.loadImage((allProduct.get(j)).getOriginal_image_url(), new SimpleImageLoadingListener());

        }

        for (int k = 0; k < allSets.size(); k++) {
            Log.e("CACHESETS " + String.valueOf(k), ((allSets.get(k)).getImage()));

            imageLoader.loadImage((allSets.get(k)).getImage(), new SimpleImageLoadingListener());

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }


    private class JsonSliderParse extends AsyncTask<String, String, JSONArray> {

        JsonParser jParser;
        JSONObject sliderItem;
        JSONArray sliderJsonArray;

        @Override
        protected void onPreExecute() {
            showAlert(context);
        }


        @Override
        protected JSONArray doInBackground(String... params) {

            jParser = new JsonParser();
            sliderJsonArray = jParser.getJSONArrayFromUrl(Constants.SLIDER_URL);
            Slider slider;

            try {
                for (int i = 0; i < sliderJsonArray.length(); i++) {
                    sliderItem = (JSONObject) sliderJsonArray.get(i);
                    slider = new Slider();
                    slider.setUrl(sliderItem.getString("url"));
                    sliderItem = (JSONObject) sliderItem.get("text");
                    slider.setText(sliderItem.getString(appLanguage.getLanguage_short()));
                    sliderImages.add(slider);
                }

            } catch (Exception e) {
                Log.e("ASNYCTASK", "DoInBackgroundError");
            }

            return sliderJsonArray;
        }


        @Override
        protected void onPostExecute(JSONArray jsonArray) {
            sliderAdapter = new SliderAdapter(context, sliderImages);
            slider.setAdapter(sliderAdapter);
            //slider.setPageTransformer(true, new RotateUpTransformer());
            catIndicator = (CirclePageIndicator) findViewById(R.id.catalog_indicator);
            catIndicator.setViewPager(slider);

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            categoryGridAdapter = new CategoryGridAdapter(context, collections);
            categoriesGrid.setAdapter(categoryGridAdapter);
            SharedPrefUtils utils = new SharedPrefUtils(context);
            utils.savePrefSlider(sliderImages);
            //utils.savePrefCollection(collections);
            disMissAlert();



            /*JsonCategoryParse categoryTask = new JsonCategoryParse();
            categoryTask.execute(Constants.CATEGORY_URL);*/
        }

    }


    private class JsonCategoryParse extends AsyncTask<String, String, JSONObject> {

        JsonParser jParser;
        JSONObject collection;
        JSONArray collectionArray;

        @Override
        protected JSONObject doInBackground(String... params) {


            jParser = new JsonParser();
            collectionArray = jParser.getJSONArrayFromUrl(Constants.CATEGORY_URL);
            Collection collectionItem;

            try {
                for (int i = 0; i < collectionArray.length(); i++) {
                    collectionItem = new Collection();
                    collection = (JSONObject) collectionArray.get(i);
                    collectionItem.setId(collection.get("id").toString());
                    collectionItem.setTitle(collection.get("title").toString());
                    collectionItem.setUrl(collection.get("image_url").toString());
                    collections.add(collectionItem);
                    Log.e("COLLECTIONITEM" + String.valueOf(i), String.valueOf(collectionItem.getTitle()));
                }

            } catch (Exception e) {
                Log.e("CategoryAsyncTask", "CategoryError");
            }

            return null;
        }


        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            //new getAllProducts().execute();
            categoryGridAdapter = new CategoryGridAdapter(context, collections);
            categoriesGrid.setAdapter(categoryGridAdapter);
            SharedPrefUtils utils = new SharedPrefUtils(context);
            utils.savePrefSlider(sliderImages);
            utils.savePrefCollection(collections);
            disMissAlert();

            //savePrefCollection(collections);
            //ArrayList myList = getPrefSlider();
            //ArrayList<Collection> myCol = utils.getPrefCollection();
            //Log.e("SizeOfMyList", String.valueOf(myCol.size()));

        }
    }


    public boolean checkCached(ArrayList<Collection> collections) {

        int control = 0;
        for (int c = 0; c < collections.size(); c++) {
            if (MemoryCacheUtils.findCachedBitmapsForImageUri(collections.get(c).getUrl(), ImageLoader.getInstance().getMemoryCache()) != null) {
                control++;
            }
        }
        if (control == collections.size()) {
            return true;
        }

        return false;
    }

    private class getAllProducts extends AsyncTask<String, String, JSONArray> {

        JsonParser jParser;
        JSONObject productsItem, collectionsItem, setsItem;
        JSONArray productsJsonArray, collectionsJsonArray, setsJsonArray;
        ProgressDialog cDialog;


        @Override
        protected void onPreExecute() {

            cDialog = null;
            cDialog = new ProgressDialog(context);
            cDialog.setMessage("Veriler Güncelleniyor Lütfen Bekleyiniz...");
            cDialog.setTitle("Yükleniyor...");
            cDialog.setCancelable(false);
            cDialog.show();
        }

        @Override
        protected JSONArray doInBackground(String... params) {

            jParser = new JsonParser();
            collectionsJsonArray = jParser.getJSONArrayFromUrl(Constants.ALLITEMS_URL);

            if (collectionsJsonArray != null) {
                Log.e("collectionsJsonArray", String.valueOf(collectionsJsonArray.length()));
            }

            Product product;
            Collection collection;
            Sets sets;

            try {

                for (int i = 0; i < collectionsJsonArray.length(); i++) {

                    collection = new Collection();
                    collectionsItem = (JSONObject) collectionsJsonArray.get(i);
                    collection.setId(collectionsItem.get("id").toString());
                    collection.setTitle(collectionsItem.get("title").toString());
                    collection.setUrl(collectionsItem.get("image").toString());
                    allCollection.add(collection);
                    productsJsonArray = collectionsItem.getJSONArray("sets");

                    if (productsJsonArray != null) {
                        Log.e("productsJsonArray" + i, String.valueOf(productsJsonArray.length()));
                    }

                    for (int j = 0; j < productsJsonArray.length(); j++) {

                        product = new Product();
                        productsItem = (JSONObject) productsJsonArray.get(j);

                        product.setId(productsItem.getString("id"));
                        product.setTitle(productsItem.getString("title"));
                        product.setDetail(productsItem.getString("detail"));
                        product.setThumb_image_url("http://www1.cottonbox.com.tr" + productsItem.getString("image"));
                        product.setOriginal_image_url("http://www1.cottonbox.com.tr" + productsItem.getString("image"));
                        product.setCollection_id(productsItem.getString("collection_id"));

                        allProduct.add(product);

                        setsJsonArray = productsItem.getJSONArray("products");

                        if (setsJsonArray != null) {
                            Log.e("setsJsonArray" + j, String.valueOf(setsJsonArray.length()));
                        }

                        for (int k = 0; k < setsJsonArray.length(); k++) {
                            sets = new Sets();
                            setsItem = (JSONObject) setsJsonArray.get(k);
                            sets.setId(setsItem.getString("id"));
                            sets.setTitle(setsItem.getString("title"));
                            sets.setDescription(setsItem.getString("detail"));

                            String value = setsItem.getString("image");

                            JSONArray array = new JSONArray(value);

                            if (array.length() > 0) {

                                Log.e("SETSITEM", String.valueOf(array.get(0)));

                                sets.setImage("http://www1.cottonbox.com.tr" + String.valueOf(array.get(0)));
                            } else {

                                sets.setImage("http://www1.cottonbox.com.tr/uploads/556f083e34541.jpg");
                            }

                            sets.setSet_id(setsItem.getString("set_id"));


                            Log.e("SETSINDEX", i + "--" + ((JSONObject) collectionsJsonArray.get(i)).getString("title") +
                                    "//" + j + "--" + ((JSONObject) productsJsonArray.get(j)).getString("title") +
                                    "//" + k + "--" + ((JSONObject) setsJsonArray.get(k)).getString("title"));

                            allSets.add(sets);


                        }
                    }
                }

            } catch (Exception e) {
                Log.e("ASNYCTASK", "ALLITEMDoInBackgroundError");
            }

            return collectionsJsonArray;
        }


        @Override
        protected void onPostExecute(JSONArray jsonArray) {


            cDialog.dismiss();
            Log.e("COLLECTIONITEM", String.valueOf(collections.get(0).getTitle()));
            categoryGridAdapter = new CategoryGridAdapter(context, collections);
            categoriesGrid.setAdapter(categoryGridAdapter);
            SharedPrefUtils utils = new SharedPrefUtils(context);
            utils.savePrefSlider(sliderImages);
            utils.savePrefCollection(collections);
            utils.savePrefProduct(allProduct);
            utils.savePrefSet(allSets);

            //new cacheTask().execute();

            Log.e("ALLCOLLECTION", String.valueOf(allCollection.size()));
            Log.e("ALLPRODUCTS", String.valueOf(allProduct.size()));
            Log.e("ALLSETS", String.valueOf(allSets.size()));


        }


    }

    private class cacheMedia extends AsyncTask<String, String, JSONArray> {
        @Override
        protected JSONArray doInBackground(String... params) {


            for (int k = 0; k < allNews.size(); k++) {
                imageLoader.loadImage(allNews.get(k).getOriginal_image_url(), new SimpleImageLoadingListener());
                Log.e("NEWS " + String.valueOf(k), "Image Cached");
            }


            return new JSONArray();
        }

        @Override
        protected void onPostExecute(JSONArray jsonArray) {


            for (int c = 0; c < allChannel.size(); c++) {
                imageLoader.loadImage((allChannel.get(c)).getImage_url(), new SimpleImageLoadingListener());
                ArrayList<Series> serieList = allChannel.get(c).getSeriesList();
                for (int s = 0; s < serieList.size(); s++) {
                    imageLoader.loadImage(serieList.get(s).getOriginal_image_url(), new SimpleImageLoadingListener());
                    //Log.e("CHANNEL", serieList.get(s).getName() + serieList.get(s).);


                    JSONArray mediaArray = null;
                    try {
                        mediaArray = new JSONArray(serieList.get(s).getJsonImages());

                        if (mediaArray.length() > 0) {
                            for (int m = 0; m < mediaArray.length(); m++) {

                                imageLoader.loadImage(Constants.MISSING_URL + String.valueOf(mediaArray.get(m)), new SimpleImageLoadingListener());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    
                   /* if (serieList.get(s).getImages() == null) {
                        Log.e("NULLNULL", "NULLL");
                    }
                    for (int m = 0; m < (serieList.get(s).getImages()).size(); m++) {
                        ImageLoader.getInstance().loadImage(serieList.get(s).getImages().get(m), new SimpleImageLoadingListener());
                    }*/

                }
            }

            new pressMedia().execute();

        }

    }


    private class pressMedia extends AsyncTask<String, String, JSONArray> {
        @Override
        protected JSONArray doInBackground(String... params) {

            for (int i = 0; i < allPress.size(); i++) {
                imageLoader.loadImage((allPress.get(i)).getOriginal_image_url(), new SimpleImageLoadingListener());
            }

            return new JSONArray();
        }

    }


}



